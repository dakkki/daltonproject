#  Copyright (C)  The Dalton Project Developers.
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#  Contact information can be found on our website: https://www.daltonproject.org/

from typing import Any, Dict, List, Optional, Union

import numpy as np


class QCMethod:
    """Specifies the QC method including all associated settings."""

    def __init__(self,
                 qc_method: str,
                 xc_functional: Optional[str] = None,
                 exchange: Optional[str] = None,
                 coulomb: Optional[str] = None,
                 scf_threshold: Optional[float] = None,
                 exact_exchange: Optional[float] = None,
                 environment: Optional[str] = None) -> None:
        """Initialize QCMethod instance.

        Args:
            qc_method: Name of quantum chemistry method.
            xc_functional: Name of exchange-correlation functional.
            exchange: Name of approximation to use for the exchange contribution.
            coulomb: Name of approximation to use for the Coulomb contribution.
            scf_threshold: SCF convergence threshold.
            environment: Name of the environment model.
        """
        self.settings: Dict[str, Any] = {}
        self.qc_method(qc_method=qc_method)
        if xc_functional is not None:
            self.xc_functional(xc_functional)
        if coulomb is not None:
            self.coulomb(coulomb)
        if exchange is not None:
            self.exchange(exchange)
        if scf_threshold is not None:
            self.scf_threshold(scf_threshold)
        if exact_exchange is not None:
            self.exact_exchange(exact_exchange)
        if environment is not None:
            self.environment(environment)

    def __repr__(self):
        # use list avoid doing string +=, since it scales quadratically in the number of elements
        repr_items = [f'{self.__class__.__name__}(']
        # used to work out padding
        last_idx = len(self.settings) - 1
        pad = ' ' * (len(self.__class__.__name__) + 1)
        for idx, (key, value) in enumerate(self.settings.items()):
            if idx == last_idx == 0:
                # if last_idx is 0 and idx is 0, then there is only a single
                # element (no padding, no comma, no newline)
                repr_items.append(f'{key}: {value}')
            elif idx == 0:
                # otherwise, we are printing the first normal element (newline and comma, but not padded)
                repr_items.append(f'{key}: {value},\n')
            elif idx == last_idx:
                # otherwise, if we arrive at last_idx (no newline and no comma)
                repr_items.append(f'{pad}{key}: {value}')
            else:
                # otherwise, we are printing a normal element (newline and comma)
                repr_items.append(f'{pad}{key}: {value},\n')
        repr_items.append(')')
        return ''.join(repr_items)

    def qc_method(self, qc_method: str) -> None:
        """Specifies quantum chemistry method.

        Args:
            qc_method: Name of quantum chemical method.
        """
        if not isinstance(qc_method, str):
            raise TypeError('QC method must be given as a string.')
        self.settings.update({'qc_method': qc_method.upper()})

    def xc_functional(self, xc_functional: str) -> None:
        """Specify exchange-correlation functional to use with DFT methods.

        Args:
            xc_functional: Name of exchange-correlation functional.
        """
        if not isinstance(xc_functional, str):
            raise TypeError('XC functional must be given as a string.')
        if self.settings['qc_method'] not in ['DFT', 'HFSRDFT', 'MP2SRDFT', 'CASSRDFT', 'CASCISRDFT']:
            raise TypeError('Specifying XC functional is only valid together with DFT methods.')
        self.settings.update({'xc_functional': xc_functional.upper()})

    def scf_threshold(self, threshold: float) -> None:
        """Specify convergence threshold for SCF calculations.

        Args:
            threshold: Threshold for SCF convergence in Hartree.
        """
        if not isinstance(threshold, float):
            raise TypeError('SCF threshold must be given as a float.')
        self.settings.update({'scf_threshold': threshold})

    def coulomb(self, coulomb: str) -> None:
        """Specify name of Coulomb approximation.

        Args:
            coulomb: Name of approximation to use for the Coulomb contribution.
        """
        self.settings.update({'coulomb': coulomb.upper()})

    def exchange(self, exchange: str) -> None:
        """Specify name of exchange approximation.

        Args:
            exchange: Name of approximation to use for the exchange contribution.
        """
        self.settings.update({'exchange': exchange.upper()})

    def environment(self, environment):
        """Specify the environment model.

        Args:
            environment: Name of the environment model.
        """
        self.settings.update({'environment': environment})

    def complete_active_space(
        self,
        num_active_electrons: int,
        num_cas_orbitals: Union[List[int], int],
        num_inactive_orbitals: Union[List[int], int],
    ) -> None:
        """Specify complete active space.

        Args:
            num_active_electrons: Number of active electrons.
                                  // Maybe this should be inferred just from the num_inactive_orbitals?
            num_cas_orbitals: List of number of orbitals in complete active space.
            num_inactive_orbitals: List of number of inactive (doubly occupied) orbitals.
        """
        if not isinstance(num_active_electrons, int):
            raise TypeError('num_active_electrons must be given as an integer.')
        if isinstance(num_inactive_orbitals, int):
            # In case of NoSymmetry the function can take in the number of inactive orbitals as an integer.
            num_inactive_orbitals = [num_inactive_orbitals]
        if isinstance(num_cas_orbitals, int):
            # In case of NoSymmetry the function can take in the number of cas orbitals as an integer.
            num_cas_orbitals = [num_cas_orbitals]
        for num_orbitals in num_inactive_orbitals:
            if not isinstance(num_orbitals, int):
                raise TypeError('num_inactive_orbitals must only be integers.')
        for num_orbitals in num_cas_orbitals:
            if not isinstance(num_orbitals, int):
                raise TypeError('num_cas_orbitals must only be integers.')
        if len(num_inactive_orbitals) != len(num_cas_orbitals):
            raise ValueError('Dimension of num_inactive_orbitals and num_cas_orbitals must be the same.')
        if num_active_electrons > np.sum(num_cas_orbitals) * 2:
            raise ValueError('Too many active electrons for the number of CAS orbitals.')
        if self.settings['qc_method'] not in ['CASSCF', 'CASCI', 'CASSRDFT', 'CASCISRDFT']:
            raise TypeError('Specifying CAS is only valid for range multi-configurational methods.')
        self.settings.update({'num_inactive_orbitals': num_inactive_orbitals})
        self.settings.update({'num_cas_orbitals': num_cas_orbitals})
        self.settings.update({'num_active_electrons': num_active_electrons})

    def range_separation_parameter(self, mu: float) -> None:
        """Specify value of range separation parameter.

        The range separation parameter is used in srDFT.

        Args:
            mu: Range separation parameter in unit of a.u.^-1.
        """
        if isinstance(mu, int):
            mu = float(mu)
        if not isinstance(mu, float):
            raise TypeError('mu, the range separation parameter, must be given as a float.')
        if self.settings['qc_method'] not in [
                'DFT',
                'HFSRDFT',
                'MP2SRDFT',
                'CASSRDFT',
                'CASCISRDFT',
        ]:
            raise TypeError('Specifying range separation parameter is only valid for range separated methods.')
        self.settings.update({'range_separation_parameter': mu})

    def input_orbital_coefficients(self, orbitals: Union[np.ndarray, Dict[int, np.ndarray]]) -> None:
        """Specify starting guess for orbitals.

        Args:
            orbitals: Orbital coefficients.
        """
        if not isinstance(orbitals, np.ndarray) and not isinstance(orbitals, dict):
            raise TypeError('orbitals must be an array or multiple arrays in a dictionary.')
        self.settings.update({'orbitals': orbitals})

    def target_state(self, state: int, symmetry: int = 1) -> None:
        """Specify target state for state-specific calculations.

        Args:
            state: Which state to be targeted, counting from 1.
            symmetry: Symmetry of the target state.
        """
        if not isinstance(state, int):
            raise TypeError('state must be specified as an integer.')
        if not isinstance(symmetry, int):
            raise TypeError('symmetry must be specified as an integer.')
        if self.settings['qc_method'] not in ['CASSCF', 'CASCI', 'CASSRDFT', 'CASCISRDFT']:
            raise TypeError('Specifying target state is only valid for state-specific methods.')
        self.settings.update({'state_number': state})
        self.settings.update({'state_symmetry': symmetry})

    def exact_exchange(self, exact_exchange: float) -> None:
        """Specify amount of Hartree-Fock-like exchange in DFT calculation.

        Args:
            exact_exchange: Coefficient specifying amount of Hartree-Fock-like exchange,
                where 0.0 is only DFT exchange and 1.0 is only Hartree-Fock-like exchange.
        """
        if not isinstance(exact_exchange, float):
            raise TypeError('Exact exchange amount must be specified as a float.')
        if not 0.0 <= exact_exchange <= 1.0:
            raise ValueError('Exact exchange amount must be between zero and one.')
        if self.settings['qc_method'] not in ['DFT', 'HFSRDFT', 'MP2SRDFT', 'CASSRDFT']:
            raise TypeError('Specifying Hartree-Fock-like exchange is only valid for DFT methods.')
        self.settings.update({'exact_exchange': exact_exchange})
