"""Classes and functions related to interfaces to executable programs.

Defines methods that are required for interfaces to external programs that will
run as executables and produce output that will be parsed."""
#  Copyright (C)  The Dalton Project Developers.
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#  Contact information can be found on our website: https://www.daltonproject.org/

import os
import socket
import tempfile
import warnings
from abc import ABC, abstractmethod
from typing import Dict, List, NamedTuple, Optional, Union

import numpy as np
import psutil

from .basis import Basis
from .environment import Environment
from .molecule import Molecule
from .property import Property
from .qcmethod import QCMethod
from .utilities import num_cpu_cores, num_numa_nodes, run


class NumOrbitals(NamedTuple):
    tot_num_orbitals: int
    num_orbitals_per_sym: List[int]


class NumBasisFunctions(NamedTuple):
    tot_num_basis_functions: int
    num_basis_functions_per_sym: List[int]


class OrbitalEnergies(NamedTuple):
    orbital_energies: np.ndarray
    orbital_energies_per_sym: Dict[str, np.ndarray]


class OrbitalEnergy(NamedTuple):
    orbital_energy: float
    symmetry: str


class OrbitalCoefficients(NamedTuple):
    orbital_coefficients: np.ndarray
    orbital_coefficients_per_sym: Dict[str, np.ndarray]


class ExcitationEnergies(NamedTuple):
    excitation_energies: np.ndarray
    excitation_energies_per_sym: Dict[str, np.ndarray]


class OscillatorStrengths(NamedTuple):
    oscillator_strengths: np.ndarray
    oscillator_strengths_per_sym: Dict[str, np.ndarray]


class HyperfineCouplings(NamedTuple):
    polarization: np.ndarray
    direct: np.ndarray
    total: np.ndarray


class PolarizabilityGradients(NamedTuple):
    frequencies: np.ndarray
    values: np.ndarray


class OutputParser(ABC):
    """Abstract OutputParser class.

     The implementation of this class defines methods for parsing output files
     produced by a program.
     """

    @property
    @abstractmethod
    def filename(self) -> str:
        pass

    @property
    @abstractmethod
    def energy(self) -> float:
        pass

    @property
    @abstractmethod
    def dipole(self) -> np.ndarray:
        pass

    @property
    @abstractmethod
    def polarizability(self) -> np.ndarray:
        pass

    @property
    @abstractmethod
    def first_hyperpolarizability(self) -> np.ndarray:
        pass

    @property
    @abstractmethod
    def electronic_energy(self) -> float:
        pass

    @property
    @abstractmethod
    def nuclear_repulsion_energy(self) -> float:
        pass

    @property
    @abstractmethod
    def num_electrons(self) -> int:
        pass

    @property
    @abstractmethod
    def num_orbitals(self) -> Union[int, NumOrbitals]:
        pass

    @property
    @abstractmethod
    def num_basis_functions(self) -> Union[int, NumBasisFunctions]:
        pass

    @property
    @abstractmethod
    def mo_energies(self) -> OrbitalEnergies:
        pass

    @property
    @abstractmethod
    def homo_energy(self) -> OrbitalEnergy:
        pass

    @property
    @abstractmethod
    def lumo_energy(self) -> OrbitalEnergy:
        pass

    @property
    @abstractmethod
    def orbital_coefficients(self) -> Union[np.ndarray, Dict[int, np.ndarray]]:
        pass

    @property
    @abstractmethod
    def natural_occupations(self) -> Union[np.ndarray, Dict[int, np.ndarray]]:
        pass

    @property
    @abstractmethod
    def gradients(self) -> np.ndarray:
        """Gradients with respect to nuclear displacements in Cartesian coordinates."""
        pass

    @property
    @abstractmethod
    def hessian(self) -> np.ndarray:
        """Second derivatives with respect to nuclear displacements in Cartesian coordinates."""
        pass

    @property
    @abstractmethod
    def dipole_gradients(self) -> np.ndarray:
        """Gradients of dipole moment with respect to nuclear displacements in Cartesian coordinates."""
        pass

    @property
    @abstractmethod
    def polarizability_gradients(self) -> PolarizabilityGradients:
        """Gradients of the polarizability with respect to nuclear displacements in Cartesian coordinates."""
        pass

    @property
    @abstractmethod
    def final_geometry(self) -> np.ndarray:
        pass

    @property
    @abstractmethod
    def excitation_energies(self) -> np.ndarray:
        pass

    @property
    @abstractmethod
    def oscillator_strengths(self) -> np.ndarray:
        pass

    @property
    @abstractmethod
    def two_photon_tensors(self) -> np.ndarray:
        pass

    @property
    @abstractmethod
    def two_photon_strengths(self) -> np.ndarray:
        pass

    @property
    @abstractmethod
    def two_photon_cross_sections(self) -> np.ndarray:
        pass

    @property
    @abstractmethod
    def hyperfine_couplings(self) -> HyperfineCouplings:
        pass


class ComputeSettings:
    """Compute settings to be used by Program.compute() implementations.

    The settings can be given as arguments during initialization. Defaults are
    used for any missing argument(s).
    """

    def __init__(self,
                 work_dir: Optional[str] = None,
                 scratch_dir: Optional[str] = None,
                 node_list: Optional[List[str]] = None,
                 jobs_per_node: Optional[int] = None,
                 mpi_command: Optional[str] = None,
                 mpi_num_procs: Optional[int] = None,
                 omp_num_threads: Optional[int] = None,
                 memory: Optional[int] = None,
                 comm_port: Optional[int] = None,
                 launcher: Optional[str] = None) -> None:
        """Initialize ComputeSettings.

        Args:
            work_dir: Work directory in which the input and output files are
                written. Default is to use current working directory.
            scratch_dir: Scratch directory used by the programs for temporary
                storage. Default is to first check environment variable
                DALTON_TMPDIR, then SCRATCH, and lastly to use the system's
                temporary directory as returned by the tempfile module.
            node_list: List of nodes. Defaults to nodes defined by PBS/Torque or
                SLURM schedulers or the current host.
            jobs_per_node: Number of concurrent jobs per node. Only used for
                job farming, i.e., for running several jobs concurrently.
                Default is one job per node.
            mpi_command: MPI command including options. Must end with the
                option that specifies number of processes. Default is to first
                check the DP_MPI_COMMAND environment variable and otherwise it
                is set to 'mpirun -np'.
            mpi_num_procs: Number of MPI processes. Defaults to the value of
                SLURM_NTASKS (if defined) or to one MPI process per NUMA node.
            omp_num_threads: Number of OpenMP threads per process. Defaults to
                the value of OMP_NUM_THREADS, SLURM_CPUS_PER_TASK, or the
                number of physical cores divided by the number of MPI
                processes.
            memory: Total amount of memory in MB per node. Defaults to all
                available memory minus 200 MB used for overhead.
            comm_port: Port used for communication between nodes. Only used for
                job farming.
            launcher: Launcher that will be used to launch programs. Note that
                specifying it will override mpi_command, mpi_num_procs, and
                omp_num_threads. However, it will not override any launcher
                environment variables that are specific to a program, e.g.,
                DALTON_LAUNCHER for Dalton and LSDALTON_LAUNCHER for LSDalton.
        """
        self._work_dir: str
        self._scratch_dir: str
        self._nodelist: List[str]
        self._num_nodes: int
        self._jobs_per_node: int
        self._mpi_command: str
        self._mpi_num_procs: int
        self._omp_num_threads: int
        self._memory: int
        self._comm_port: int
        self._launcher: Optional[str]
        if work_dir is None:
            work_dir = os.getcwd()
        self.work_dir = work_dir
        if scratch_dir is None:
            if 'DALTON_TMPDIR' in os.environ:
                scratch_dir = os.environ['DALTON_TMPDIR']
            elif 'SCRATCH' in os.environ:
                scratch_dir = os.environ['SCRATCH']
            else:
                scratch_dir = tempfile.gettempdir()
        self.scratch_dir = scratch_dir
        if node_list is None:
            if 'PBS_NODEFILE' in os.environ:
                with open(os.environ['PBS_NODEFILE']) as node_file:
                    node_list = node_file.read().splitlines()
            elif 'SLURM_NODELIST' in os.environ:
                command = f'scontrol show hostname {os.environ["SLURM_NODELIST"]}'
                stdout, stderr, return_code = run(command)
                if return_code != 0:
                    raise Exception(f'Error fetching node_list from Slurm. Error message:\n{stderr}')
                node_list = stdout.split()
            else:
                node_list = [socket.gethostname()]
        self.node_list = node_list
        if jobs_per_node is None:
            jobs_per_node = 1
        self.jobs_per_node = jobs_per_node
        if mpi_command is None:
            if 'DP_MPI_COMMAND' in os.environ:
                mpi_command = os.environ['DP_MPI_COMMAND']
            else:
                mpi_command = 'mpirun -np'
        self._mpi_command = mpi_command
        if mpi_num_procs is None:
            if 'SLURM_NTASKS' in os.environ:
                mpi_num_procs = int(os.environ['SLURM_NTASKS'])
            else:
                mpi_num_procs = num_numa_nodes() * self.num_nodes
        self.mpi_num_procs = mpi_num_procs
        if omp_num_threads is None:
            if 'OMP_NUM_THREADS' in os.environ:
                omp_num_threads = int(os.environ['OMP_NUM_THREADS'])
            elif 'SLURM_CPUS_PER_TASK' in os.environ:
                omp_num_threads = int(os.environ['SLURM_CPUS_PER_TASK'])
            else:
                omp_num_threads = num_cpu_cores() // (self.mpi_num_procs // self.num_nodes)
        self.omp_num_threads = omp_num_threads
        if memory is None:
            # Get available memory in bytes, convert to megabytes, and subtract 200 MB to allow overhead.
            memory = psutil.virtual_memory().available // 1e6 - 200
        self.memory = memory
        if comm_port is None:
            try:
                with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
                    sock.bind(('', 0))
                    comm_port = sock.getsockname()[1]
            except OSError:
                warnings.warn('Unable to find open port.')
        self.comm_port = comm_port
        self.launcher = launcher

    def __repr__(self):
        repr_str = (f'ComputeSettings(work_dir={self.work_dir},'
                    f' scratch_dir={self.scratch_dir},'
                    f' mpi_command={self.mpi_command},'
                    f' mpi_num_procs={self.mpi_num_procs},'
                    f' omp_num_threads={self.omp_num_threads},'
                    f' memory={self.memory},'
                    f' node_list={self.node_list},'
                    f' jobs_per_node={self.jobs_per_node},'
                    f' comm_port={self.comm_port},'
                    f' launcher={self.launcher})')
        return repr_str

    @property
    def work_dir(self) -> str:
        """Work directory."""
        return self._work_dir

    @work_dir.setter
    def work_dir(self, work_dir: str) -> None:
        self._work_dir = work_dir

    @property
    def scratch_dir(self) -> str:
        """Scratch directory."""
        return self._scratch_dir

    @scratch_dir.setter
    def scratch_dir(self, scratch_dir: str) -> None:
        self._scratch_dir = scratch_dir

    @property
    def node_list(self) -> List[str]:
        """List of nodes."""
        return self._nodelist

    @node_list.setter
    def node_list(self, nodelist: List[str]) -> None:
        self._nodelist = list(set(nodelist))

    @property
    def num_nodes(self):
        """Number of nodes."""
        return len(self.node_list)

    @property
    def jobs_per_node(self) -> int:
        """Jobs per node."""
        return self._jobs_per_node

    @jobs_per_node.setter
    def jobs_per_node(self, jobs_per_node: int) -> None:
        self._jobs_per_node = jobs_per_node

    @property
    def mpi_command(self) -> str:
        """MPI command."""
        return self._mpi_command

    @mpi_command.setter
    def mpi_command(self, mpi_command: str) -> None:
        self._mpi_command = mpi_command

    @property
    def mpi_num_procs(self) -> int:
        """Number of MPI processes."""
        return self._mpi_num_procs

    @mpi_num_procs.setter
    def mpi_num_procs(self, mpi_num_procs: int) -> None:
        self._mpi_num_procs = int(mpi_num_procs)

    @property
    def omp_num_threads(self) -> int:
        """Number of OpenMP threads (per MPI process)."""
        return self._omp_num_threads

    @omp_num_threads.setter
    def omp_num_threads(self, omp_num_threads: int) -> None:
        self._omp_num_threads = int(omp_num_threads)

    @property
    def memory(self) -> int:
        """Total amount of memory in MB per node."""
        return self._memory

    @memory.setter
    def memory(self, memory: int) -> None:
        self._memory = int(memory)

    @property
    def comm_port(self):
        """Communication port."""
        return self._comm_port

    @comm_port.setter
    def comm_port(self, comm_port: int) -> None:
        self._comm_port = comm_port

    @property
    def launcher(self) -> Optional[str]:
        """Launcher command."""
        return self._launcher

    @launcher.setter
    def launcher(self, launcher: Optional[str]) -> None:
        self._launcher = launcher


class Program(ABC):
    """Program base class to enforce program interface.

    The interface must implement a `compute()` method that prepares and
    executes a calculation, and returns an instance of the OutputParser class.
    """

    @classmethod
    @abstractmethod
    def compute(cls,
                molecule: Molecule,
                basis: Basis,
                qc_method: QCMethod,
                properties: Property,
                environment: Optional[Environment] = None,
                compute_settings: Optional[ComputeSettings] = None,
                filename: Optional[str] = None,
                force_recompute: bool = False) -> OutputParser:
        """
        Args:
            molecule: Molecule on which a calculations is performed. This can
                also be an atom, a fragment, or any collection of atoms.
            basis: Basis set to use in the calculation.
            qc_method: Quantum chemistry method, e.g., HF, DFT, or CC, and
                associated settings.
            properties: Properties of molecule to be calculated, geometry
                optimization, excitation energies, etc.
            environment: TODO....
            compute_settings: Settings for the calculation, e.g., number of MPI
                processes and OpenMP threads, work and scratch directory, etc.
            filename: Optional user-specified filename that will be used for
                input and output files. If not specified a name will be
                generated as a hash of the input.
            force_recompute: Recompute even if the output files already exist.

        Returns:
            OutputParser instance with a reference to the filename used in the
            calculation.
        """
        pass

    @staticmethod
    def _validate_compute_args(molecule: Molecule,
                               basis: Basis,
                               qc_method: QCMethod,
                               properties: Property,
                               environment: Optional[Environment] = None,
                               compute_settings: Optional[ComputeSettings] = None,
                               filename: Optional[str] = None,
                               force_recompute: Optional[bool] = None) -> None:
        """Check that arguments to Program.compute.__init__ are correct type."""
        if not isinstance(molecule, Molecule):
            raise TypeError('Argument "molecule" must be an instance of the Molecule class.')
        if not isinstance(basis, Basis):
            raise TypeError('Argument "basis" must be an instance of the Basis class.')
        if not isinstance(qc_method, QCMethod):
            raise TypeError('Argument "qc_method" must be a instance of the QCMethod class.')
        if not isinstance(properties, Property):
            raise TypeError('Argument "properties" must be an instance of the Property class.')
        if environment:
            if not isinstance(environment, Environment):
                raise TypeError('Argument "environment" must be an instance of the Environment class.')
        if compute_settings:
            if not isinstance(compute_settings, ComputeSettings):
                raise TypeError('Argument "compute_settings" must be an instance of ComputeSettings.')
        if filename:
            if not isinstance(filename, str):
                raise TypeError('Argument "filename" must be of type str.')
        if force_recompute:
            if not isinstance(force_recompute, bool):
                raise TypeError('Argument "force_recompute" must be of type bool.')
