#  Copyright (C)  The Dalton Project Developers.
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#  Contact information can be found on our website: https://www.daltonproject.org/

import os
from typing import Optional, Tuple

import numpy as np

from ..basis import Basis
from ..molecule import Molecule
from ..property import Property
from ..qcmethod import QCMethod
from ..utilities import unique_filename
from .program import lsdalton_input, molecule_input

try:
    import pylsdalton
except ImportError:
    pylsdalton = None
else:
    # TODO perform some sanity checks, like test version compatibility?
    pass

__all__ = (
    'num_atoms',
    'num_electrons',
    'num_basis_functions',
    'num_ri_basis_functions',
    'overlap_matrix',
    'kinetic_energy_matrix',
    'nuclear_electron_attraction_matrix',
    'coulomb_matrix',
    'exchange_matrix',
    'exchange_correlation',
    'fock_matrix',
    'eri',
    'eri4',
    'ri3',
    'ri2',
    'diagonal_density',
)

_STATE: Optional[str] = None


def num_atoms(molecule: Molecule, basis: Basis, qc_method: QCMethod) -> int:
    """Return the number of atoms.

    Args:
        molecule: Molecule object.
        basis: Basis object.
        qc_method: QCMethod object.

    Returns:
        Number of atoms
    """
    if not pylsdalton:
        raise Exception('PyLSDalton is not available.')
    _set_lsdalton_state(molecule, basis, qc_method)
    return pylsdalton.number_atoms()


def num_electrons(molecule: Molecule, basis: Basis, qc_method: QCMethod) -> int:
    """Return the number of electrons.

    Args:
        molecule: Molecule object.
        basis: Basis object.
        qc_method: QCMethod object.

    Returns:
        Number of electrons
    """
    if not pylsdalton:
        raise Exception('PyLSDalton is not available.')
    _set_lsdalton_state(molecule, basis, qc_method)
    return pylsdalton.number_electrons()


def num_basis_functions(molecule: Molecule, basis: Basis, qc_method: QCMethod) -> int:
    """Return the number of basis functions.

    Args:
        molecule: Molecule object.
        basis: Basis object.
        qc_method: QCMethod object.

    Returns:
        Number of basis functions
    """
    if not pylsdalton:
        raise Exception('PyLSDalton is not available.')
    _set_lsdalton_state(molecule, basis, qc_method)
    return pylsdalton.number_basis()


def num_ri_basis_functions(molecule: Molecule, basis: Basis, qc_method: QCMethod) -> int:
    """Returns the number of auxiliary (RI) basis functions.

    Args:
        molecule: Molecule object.
        basis: Basis object.
        qc_method: QCMethod object.

    Returns:
        Number of auxiliary (RI) basis functions
    """
    if not pylsdalton:
        raise Exception('PyLSDalton is not available.')
    _set_lsdalton_state(molecule, basis, qc_method)
    return pylsdalton.number_aux()


def overlap_matrix(molecule: Molecule, basis: Basis, qc_method: QCMethod) -> np.ndarray:
    """Calculate the overlap matrix.

    Args:
        molecule: Molecule object.
        basis: Basis object.
        qc_method: QCMethod object.

    Returns:
        Overlap matrix
    """
    if not pylsdalton:
        raise Exception('PyLSDalton is not available.')
    _set_lsdalton_state(molecule, basis, qc_method)
    return pylsdalton.overlap_matrix()


def kinetic_energy_matrix(molecule: Molecule, basis: Basis, qc_method: QCMethod) -> np.ndarray:
    """Calculate the kinetic energy matrix.

    Args:
        molecule: Molecule object.
        basis: Basis object.
        qc_method: QCMethod object.

    Returns:
        Kinetic energy matrix
    """
    if not pylsdalton:
        raise Exception('PyLSDalton is not available.')
    _set_lsdalton_state(molecule, basis, qc_method)
    return pylsdalton.kinetic_energy_matrix()


def nuclear_electron_attraction_matrix(molecule: Molecule, basis: Basis, qc_method: QCMethod) -> np.ndarray:
    """Calculate the nuclear-electron attraction matrix.

    Args:
        molecule: Molecule object.
        basis: Basis object.
        qc_method: QCMethod object.

    Returns:
        Nuclear-electron attraction matrix
    """
    if not pylsdalton:
        raise Exception('PyLSDalton is not available.')
    _set_lsdalton_state(molecule, basis, qc_method)
    return pylsdalton.nuclear_electron_attraction_matrix()


def coulomb_matrix(density_matrix: np.ndarray, molecule: Molecule, basis: Basis,
                   qc_method: QCMethod) -> np.ndarray:
    """Calculate the Coulomb matrix.

    Args:
        density_matrix: Density matrix
        molecule: Molecule object.
        basis: Basis object.
        qc_method: QCMethod object.

    Returns:
        Coulomb matrix
    """
    if not pylsdalton:
        raise Exception('PyLSDalton is not available.')
    _set_lsdalton_state(molecule, basis, qc_method)
    return pylsdalton.coulomb_matrix(density_matrix)


def exchange_matrix(density_matrix: np.ndarray, molecule: Molecule, basis: Basis,
                    qc_method: QCMethod) -> np.ndarray:
    """Calculate the exchange matrix.

    Args:
        density_matrix: Density matrix.
        molecule: Molecule object.
        basis: Basis object.
        qc_method: QCMethod object.

    Returns:
        Exchange matrix
    """
    if not pylsdalton:
        raise Exception('PyLSDalton is not available.')
    _set_lsdalton_state(molecule, basis, qc_method)
    return -pylsdalton.exchange_matrix(density_matrix, dsym=True, testElectrons=False)


def exchange_correlation(density_matrix: np.ndarray, molecule: Molecule, basis: Basis,
                         qc_method: QCMethod) -> Tuple[float, np.ndarray]:
    """Calculate the exchange-correlation energy and matrix.

    Args:
        density_matrix: Density matrix.
        molecule: Molecule object.
        basis: Basis object.
        qc_method: QCMethod object.

    Returns:
        Exchange-correlation energy and matrix
    """
    if not pylsdalton:
        raise Exception('PyLSDalton is not available.')
    _set_lsdalton_state(molecule, basis, qc_method)
    return pylsdalton.exchange_correlation_matrix(density_matrix, testElectrons=False)


def fock_matrix(density_matrix: np.ndarray, molecule: Molecule, basis: Basis, qc_method: QCMethod) -> np.ndarray:
    """Calculate the Fock/KS matrix.

    Args:
        density_matrix: Density matrix
        molecule: Molecule object.
        basis: Basis object.
        qc_method: QCMethod object.

    Returns:
        Fock/KS matrix
    """
    if not pylsdalton:
        raise Exception('PyLSDalton is not available.')
    _set_lsdalton_state(molecule, basis, qc_method)
    return pylsdalton.fock_matrix(density_matrix, testElectrons=False)


def eri(specs: str, molecule: Molecule, basis: Basis, qc_method: QCMethod) -> np.ndarray:
    r"""Calculate electron-repulsion integrals for different operator choices.

    .. math::
        g_{ijkl} = \int\mathrm{d}\mathbf{r}\int\mathrm{d}\mathbf{r}^{\prime}
            \chi_{i}(\mathbf{r})\chi_{j}(\mathbf{r}^\prime)
            g(\mathbf{r},\mathbf{r}^\prime)
            \chi_{k}(\mathbf{r})\chi_{l}(\mathbf{r}^\prime)

    The first character of the `specs` string specifies the operator
    :math:`g(\mathbf{r}, \mathbf{r}^\prime)`. Valid values are:

        * C for Coulomb (:math:`g(\mathbf{r}, \mathbf{r}^\prime) = \frac{1}{|\mathbf{r} - \mathbf{r}^\prime|}`)
        * G for Gaussian geminal (:math:`g`)
        * F geminal divided by the Coulomb operator (:math:`\frac{g}{|\mathbf{r} - \mathbf{r}^\prime|}`)
        * D double commutator (:math:`[[T,g],g]`)
        * 2 Gaussian geminal operator squared (:math:`g^2`)

    The last four characters of the `specs` string specify the AO basis to use
    for the four basis functions :math:`\chi_{i}`, :math:`\chi_{j}`,
    :math:`\chi_{k}`, and :math:`\chi_{l}`. Valid values are:

        * R for regular AO basis
        * D for auxiliary (RI) AO basis
        * E for empty AO basis (i.e. for 2- and 3-center ERIs)
        * N for nuclei

    Args:
        specs: A 5-character-long string with the specification for AO basis
            and operator to use (see description above).
        molecule: Molecule object.
        basis: Basis object.
        qc_method: QCMethod object.

    Returns:
        Electron-repulsion integral tensor.
    """
    if not pylsdalton:
        raise Exception('PyLSDalton is not available.')
    _set_lsdalton_state(molecule, basis, qc_method)
    specs_fortran = specs[::-1]
    return pylsdalton.eri(specs_fortran)


def eri4(molecule: Molecule, basis: Basis, qc_method: QCMethod) -> np.ndarray:
    r"""Calculate four-center electron-repulsion integrals.

    .. math::
        (ab|cd) = g_{acbd}

    with

    .. math::
      g(\mathbf{r},\mathbf{r}^\prime) = \frac{1}{|\mathbf{r}-\mathbf{r}^\prime|}

    Args:
        molecule: Molecule object.
        basis: Basis object.
        qc_method: QCMethod object.

    Returns:
        Four-center electron-repulsion integral tensor.
    """
    if not pylsdalton:
        raise Exception('PyLSDalton is not available.')
    return eri('CRRRR', molecule, basis, qc_method)


def ri3(molecule: Molecule, basis: Basis, qc_method: QCMethod) -> np.ndarray:
    r"""Calculate three-center electron-repulsion integrals.

    .. math::
        (ab|I) = g_{a0bI}

    with

    .. math::
        g(\mathbf{r},\mathbf{r}^\prime) = \frac{1}{|\mathbf{r}-\mathbf{r}^\prime|}

    Args:
        molecule: Molecule object.
        basis: Basis object.
        qc_method: QCMethod object.

    Returns:
        Three-center RI integrals
    """
    if not pylsdalton:
        raise Exception('PyLSDalton is not available.')
    return eri('CRRDE', molecule, basis, qc_method)


def ri2(molecule: Molecule, basis: Basis, qc_method: QCMethod) -> np.ndarray:
    r"""Calculate two-center electron-repulsion integrals.

    .. math::

        (I|J) = g_{I00J}

    with

    .. math::

        g(\mathbf{r},\mathbf{r}^\prime) = \frac{1}{|\mathbf{r}-\mathbf{r}^\prime|}

    Args:
        molecule: Molecule object.
        basis: Basis object.
        qc_method: QCMethod object.

    Returns:
        Two-center RI integrals
    """
    if not pylsdalton:
        raise Exception('PyLSDalton is not available.')
    return eri('CDEDE', molecule, basis, qc_method)


def diagonal_density(hamiltonian: np.ndarray, metric: np.ndarray, molecule: Molecule, basis: Basis,
                     qc_method: QCMethod) -> np.ndarray:
    """Form an AO density matrix from occupied MOs through diagonalization.

    Args:
        hamiltonian: Hamiltonian matrix.
        metric: Metric matrix (i.e. overlap matrix)
        molecule: Molecule object.
        basis: Basis object.
        qc_method: QCMethod object.

    Returns:
        Density matrix
    """
    if not pylsdalton:
        raise Exception('PyLSDalton is not available.')
    _set_lsdalton_state(molecule, basis, qc_method)
    return pylsdalton.diagonal_density(hamiltonian, metric)


def _set_lsdalton_state(molecule: Molecule, basis: Basis, qc_method: QCMethod) -> None:
    """Control the PyLSDalton instance.

    Create instance if it does not already exist. If it does exist, then ensure
    that the input is the same, otherwise destroy it and create a new instance.

    Args:
        molecule: Molecule object.
        basis: Basis object.
        qc_method: QCMethod object.
    """
    if not pylsdalton:
        raise Exception('PyLSDalton is not available.')
    global _STATE
    if not isinstance(molecule, Molecule):
        raise TypeError('molecule must be an instance of the Molecule class')
    if not isinstance(basis, Basis):
        raise TypeError('basis must be an instance of the Basis class')
    if not isinstance(qc_method, QCMethod):
        raise TypeError('qc_method must be a instance of the QCMethod class')
    properties = Property()
    dal_input = lsdalton_input(qc_method, properties)
    mol_input = molecule_input(molecule, basis)
    # TODO temporary fix until PyLSDalton uses CWD to find basis sets
    os.environ['BASDIR'] = os.getcwd()
    basis.write()
    filename = unique_filename([dal_input, mol_input])
    if _STATE != filename:
        if _STATE is not None:
            pylsdalton.destroy()
        _STATE = filename
        with open(f'{filename}.mol', 'w') as mol_file:
            mol_file.write(mol_input)
        with open(f'{filename}.dal', 'w') as dal_file:
            dal_file.write(dal_input)
        pylsdalton.create(dalfile=f'{filename}.dal', molfile=f'{filename}.mol')
