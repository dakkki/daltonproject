"""Interface to the LSDalton program that sets up and runs calculations through subprocess."""
#  Copyright (C)  The Dalton Project Developers.
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#  Contact information can be found on our website: https://www.daltonproject.org/

import logging
import os
import shutil
import warnings
from typing import List, Optional

from qcelemental import periodictable

from ..basis import Basis, get_atom_basis
from ..environment import Environment
from ..molecule import Molecule
from ..program import ComputeSettings, Program
from ..property import Property
from ..qcmethod import QCMethod
from ..utilities import chdir, job_farm, run, unique_filename
from .output_parser import OutputParser

logger = logging.getLogger('daltonproject')


class LSDalton(Program):
    """Interface to the LSDalton program."""

    program_name: str = 'lsdalton'

    @classmethod
    def compute(cls,
                molecule: Molecule,
                basis: Basis,
                qc_method: QCMethod,
                properties: Property,
                environment: Optional[Environment] = None,
                compute_settings: Optional[ComputeSettings] = None,
                filename: Optional[str] = None,
                force_recompute: Optional[bool] = False) -> OutputParser:
        """Run a calculation using the LSDalton program.

        Args:
            molecule: Molecule on which a calculations is performed. This can
                also be an atom, a fragment, or any collection of atoms.
            basis: Basis set to use in the calculation.
            qc_method: Quantum chemistry method, e.g., HF, DFT, or CC, and
                associated settings.
            properties: Properties of molecule to be calculated, geometry
                optimization, excitation energies, etc.
            environment: TODO Environment...
            compute_settings: Settings for the calculation, e.g., number of MPI
                processes and OpenMP threads, work and scratch directory, etc.
            filename: Optional user-specified filename that will be used for
                input and output files. If not specified a name will be
                generated as a hash of the input.
            force_recompute: Recompute even if the output files already exist.

        Returns:
            OutputParser instance that contains the filename of the output
            produced in the calculation and can be used to extract results from
            the output.
        """
        Program._validate_compute_args(molecule, basis, qc_method, properties, environment, compute_settings,
                                       filename, force_recompute)
        if compute_settings is None:
            compute_settings = ComputeSettings()
        mol_input = molecule_input(molecule, basis)
        dal_input = lsdalton_input(qc_method, properties)
        if environment:
            raise NotImplementedError('No environments implemented for LSDalton.')
        if not filename:
            input_list: List[str] = [dal_input, mol_input]
            filename = unique_filename(input_list)
        filepath = os.path.join(compute_settings.work_dir, filename)
        if os.path.isfile(f'{filepath}.out') and os.path.isfile(f'{filepath}.tar.gz') and not force_recompute:
            logger.info('Output files already exist and will be reused.')
            return OutputParser(filepath)
        if shutil.which(cmd=cls.program_name) is None:
            raise FileNotFoundError(f'The {cls.program_name} script was not found or is not executable.')
        if 'LSDALTON_LAUNCHER' in os.environ:
            lsdalton_launcher = os.environ['LSDALTON_LAUNCHER']
        elif compute_settings.launcher is not None:
            lsdalton_launcher = compute_settings.launcher
        else:
            lsdalton_launcher = f'env OMP_NUM_THREADS={compute_settings.omp_num_threads}'
            if compute_settings.mpi_num_procs > 1:
                lsdalton_launcher += f' {compute_settings.mpi_command} {compute_settings.mpi_num_procs}'
        with chdir(compute_settings.work_dir):
            with open(f'{filename}.mol', 'w') as mol_file:
                mol_file.write(mol_input)
            with open(f'{filename}.dal', 'w') as dal_file:
                dal_file.write(dal_input)
            basis.write()
            get_files: List[str] = []
            if '**OPTIMIZE' in dal_input:
                get_files.append('molecule_out.xyz')
            if '**OPENRSP' in dal_input:
                get_files.append('rsp_tensor')
            command = (f'env LSDALTON_LAUNCHER="{lsdalton_launcher}"'
                       f' {cls.program_name}'
                       f' -nobackup'
                       f' -t {compute_settings.scratch_dir}'
                       f' -mol {filename}.mol'
                       f' -dal {filename}.dal'
                       f' -o {filename}.out')
            if get_files:
                command += f' -get "{" ".join(get_files)}"'
            stdout, stderr, return_code = run(command)
            if return_code != 0:
                # TODO perform error checking
                raise Exception(f'LSDalton exited with non-zero return code. Error message:\n{stderr}')
        return OutputParser(filepath)

    @classmethod
    def compute_farm(cls,
                     molecules: List[Molecule],
                     basis: Basis,
                     qc_method: QCMethod,
                     properties: Property,
                     compute_settings: ComputeSettings = None,
                     filenames: List[str] = None,
                     force_recompute: bool = False) -> List[OutputParser]:
        """Run a series of calculations using the LSDalton program.

        Args:
            molecules: List of molecules on which calculations are performed.
            basis: Basis set to use in the calculations.
            qc_method: Quantum chemistry method, e.g., HF, DFT, or CC, and
                associated settings used for all calculations.
            properties: Properties of molecule to be calculated, geometry
                optimization, excitation energies, etc., computed for all
                molecules.
            compute_settings: Settings for the calculations, e.g., number of
                MPI processes and OpenMP threads, work and scratch directory,
                and more.
            filenames: Optional list of user-specified filenames that will be
                used for input and output files. If not specified names will be
                generated as a hash of the individual inputs.
            force_recompute: Recompute even if the output files already exist.

        Returns:
            List of OutputParser instances each of which contains the filename
            of the output produced in the calculation and can be used to
            extract results from the output.
        """
        mol_inputs = [molecule_input(molecule, basis) for molecule in molecules]
        dal_input = lsdalton_input(qc_method, properties)
        if compute_settings is None:
            compute_settings = ComputeSettings()
        if not filenames:
            input_lists = [[dal_input, mol_input] for mol_input in mol_inputs]
            filenames = [unique_filename(input_list) for input_list in input_lists]
        if 'LSDALTON_LAUNCHER' in os.environ:
            lsdalton_launcher = os.environ['LSDALTON_LAUNCHER']
        elif compute_settings.launcher is not None:
            lsdalton_launcher = compute_settings.launcher
        else:
            lsdalton_launcher = f'env OMP_NUM_THREADS={compute_settings.omp_num_threads}'
            if compute_settings.mpi_num_procs > 1:
                mpi_procs_per_job = compute_settings.mpi_num_procs // (compute_settings.jobs_per_node
                                                                       * compute_settings.num_nodes)
                lsdalton_launcher += f' {compute_settings.mpi_command} {mpi_procs_per_job}'
        os.environ['DP_VARIABLES'] = f'LSDALTON_LAUNCHER="{lsdalton_launcher}"'
        with chdir(compute_settings.work_dir):
            commands = []
            output_parsers = []
            for filename, mol_input in zip(filenames, mol_inputs):
                output_parsers.append(OutputParser(os.path.join(compute_settings.work_dir, filename)))
                if os.path.isfile(f'{filename}.out') and os.path.isfile(f'{filename}.tar.gz') and not force_recompute:
                    continue
                if shutil.which(cmd=cls.program_name) is None:
                    raise FileNotFoundError(f'The {cls.program_name} script was not found or is not executable.')
                with open(f'{filename}.mol', 'w') as mol_file:
                    mol_file.write(mol_input)
                with open(f'{filename}.dal', 'w') as dal_file:
                    dal_file.write(dal_input)
                basis.write()
                get_files: List[str] = []
                if '**OPTIMIZE' in dal_input:
                    get_files.append('molecule_out.xyz')
                if '**OPENRSP' in dal_input:
                    get_files.append('rsp_tensor')
                command = (f'{cls.program_name}'
                           f' -nobackup'
                           f' -t {compute_settings.scratch_dir}'
                           f' -mol {filename}.mol'
                           f' -dal {filename}.dal'
                           f' -o {filename}.out')
                if get_files:
                    command += f' -get "{" ".join(get_files)}"'
                commands.append(command)
            results = job_farm(commands, compute_settings.node_list, compute_settings.jobs_per_node,
                               compute_settings.comm_port)
            for stdout, stderr, return_code in results:
                if return_code != 0:
                    warnings.warn(f'LSDalton exited with non-zero return code. Error message:\n{stderr}')
        os.environ.pop('DP_VARIABLES')
        return output_parsers


def molecule_input(molecule: Molecule, basis: Basis) -> str:
    """Generate LSDalton molecule file input as a string.

    Args:
      molecule: Molecule object.
      basis: Basis object.
    """
    atom_bases = get_atom_basis(basis.basis, molecule.num_atoms, molecule.labels)
    # auxiliary RI basis used for density-fitting/RI
    if basis.ri:
        ri_bases = get_atom_basis(basis.ri, molecule.num_atoms, molecule.labels)
    else:
        ri_bases = [''] * molecule.num_atoms
    # auxiliary ADMM basis used for ADMM
    if basis.admm:
        admm_bases = get_atom_basis(basis.admm, molecule.num_atoms, molecule.labels)
    else:
        admm_bases = [''] * molecule.num_atoms
    atom_types = 0
    coordinate_groups = []
    coordinate_group = []
    element_group = []
    basis_group = []
    ri_basis_group = []
    admm_basis_group = []
    previous_element = None
    previous_basis = None
    for element, coordinate, atom_basis, ri_basis, admm_basis in zip(molecule.elements, molecule.coordinates,
                                                                     atom_bases, ri_bases, admm_bases):
        if element != previous_element or atom_basis != previous_basis:
            coordinate_group = [coordinate]
            coordinate_groups.append(coordinate_group)
            element_group.append(element)
            basis_group.append(atom_basis)
            ri_basis_group.append(ri_basis)
            admm_basis_group.append(admm_basis)
            atom_types += 1
        else:
            coordinate_group.append(coordinate)
        previous_element = element
        previous_basis = atom_basis
    mol_input = 'ATOMBASIS\n'
    mol_input += 'Generated by Dalton Project\n'
    mol_input += '\n'
    mol_input += f'Atomtypes={atom_types} Charge={molecule.charge} Angstrom\n'
    for atom_basis, ri_basis, admm_basis, element, coordinate_group in zip(basis_group, ri_basis_group,
                                                                           admm_basis_group, element_group,
                                                                           coordinate_groups):
        mol_input += f'Charge={periodictable.to_atomic_number(element):.1f}' \
                     f' Atoms={len(coordinate_group)} Basis={atom_basis}'
        if ri_basis:
            mol_input += f' Aux={ri_basis}'
        if admm_basis:
            mol_input += f' ADMM={admm_basis}'
        mol_input += '\n'
        for coordinate in coordinate_group:
            mol_input += f'{element:2} {coordinate[0]:12.6f} {coordinate[1]:12.6f} {coordinate[2]:12.6f}\n'
    return mol_input


def write_molecule_input(molecule: Molecule, basis: Basis, filename: str) -> None:
    """Write LSDalton molecule input to file.

    The string for the file is generated in molecule_input, this is to ease
    testing of correct Dalton input generation.

    Args:
        molecule: Molecule object.
        basis: Basis object.
        filename: Name of LSDalton molecule file without file extension.
    """
    with open(f'{filename}.mol', 'w') as mol_file:
        mol_file.write(molecule_input(molecule, basis))


def lsdalton_input(qc_method: QCMethod, properties: Property) -> str:
    """Function for generating LSDalton input as a string.

    Args:
        qc_method: QCMethod object.
        properties: Property object.

    Returns:
        String of LSDalton input.
    """
    # **GENERAL
    dal_input = '**GENERAL\n'
    dal_input += '.NOGCBASIS\n'
    dal_input += '.TIME\n'
    # The functional library can either be XCFUN or PSFUN. Defaults to PSFUN
    funlib = ''
    if 'funlib' in qc_method.settings:
        if qc_method.settings['funlib'] == 'xcfun':
            dal_input += '.XCFUN\n'
    if 'funlib' == '':
        funlib = '.PSFUN\n'
    dal_input += funlib

    # **WAVE FUNCTION
    wave_function = '**WAVE FUNCTION\n'
    # Defaults to VanLenthe optimization
    dens_opt = '*DENSOPT\n.VanLenthe\n'
    if qc_method.settings['qc_method'] == 'HF':
        wave_function += '.HF\n'
    elif qc_method.settings['qc_method'] == 'DFT':
        if 'exact_exchange' in qc_method.settings:
            raise NotImplementedError('Exact exchange cannot be changed for LSDalton through Dalton Project.')
        wave_function += '.DFT\n'
        wave_function += f'{qc_method.settings["xc_functional"]}\n'
    if 'scf_threshold' in qc_method.settings:
        # *DENSOPT
        if dens_opt == '':
            dens_opt += '*DENSOPT\n'
        dens_opt += '.CONVTHR\n'
        dens_opt += f'{qc_method.settings["scf_threshold"]}\n'
    if 'geometry_optimization' in properties.settings:
        # *DENSOPT
        if dens_opt == '':
            dens_opt += '*DENSOPT\n'
        dens_opt += '.RESTART\n'
    dal_input += wave_function + dens_opt

    # **INTEGRALS
    integrals = ''
    if 'coulomb' in qc_method.settings:
        if qc_method.settings['coulomb'] == 'DF':
            if integrals == '':
                integrals += '**INTEGRALS\n'
            integrals += '.DENSFIT\n'
    if 'exchange' in qc_method.settings:
        if qc_method.settings['exchange'] == 'ADMM':
            if integrals == '':
                integrals += '**INTEGRALS\n'
            integrals += '.ADMM\n'
    use_openrsp = False
    if 'hessian' in properties.settings:
        use_openrsp = True
    if 'dipole_gradients' in properties.settings:
        use_openrsp = True
    if 'polarizability_gradients' in properties.settings:
        use_openrsp = True
    if use_openrsp:
        if integrals == '':
            integrals += '**INTEGRALS\n'
        integrals += '.NOFAMILY\n'
    dal_input += integrals

    # **RESPONS
    response = ''
    molgra = ''
    tpa = ''
    if 'gradients' in properties.settings:
        if response == '':
            response += '**RESPONS\n'
        molgra += '*MOLGRA\n'
    if 'excitation_energies' in properties.settings:
        if response == '':
            response += '**RESPONS\n'
        response += '.NEXCIT\n'
        if len(properties.settings['excitation_energies']) == 1:
            response += f"{' '.join(map(str, properties.settings['excitation_energies']))}\n"
        else:
            raise ValueError('LSDalton does not support molecular symmetry. Number of states must be an integer.')
    if 'two_photon_absorption' in properties.settings:
        raise NotImplementedError('Two-photon absorption not available for LSDalton.')
    dal_input += response + molgra + tpa

    # **OPTIMIZE
    geometry_optimize = ''
    if 'geometry_optimization' in properties.settings:
        geometry_optimize = '**OPTIMIZE\n'
        # Turns off HOBIT back-transformation, which fails for linear molecules.
        geometry_optimize += '.NOHOPE\n'
    dal_input += geometry_optimize

    # **OPENRSP
    openrsp = ''
    hessian = ''
    dip_grad = ''
    pol_grad = ''
    solver = ''
    if 'hessian' in properties.settings:
        if openrsp == '':
            openrsp += '**OPENRSP\n'
            openrsp += '*RESPONSE FUNCTION\n'
        hessian += '$PROPERTY\n'
        hessian += '.PERTURBATION\n'
        hessian += '2\n'
        hessian += 'GEO\n'
        hessian += 'GEO\n'
        hessian += '.KN-RULE\n'
        hessian += '0\n'
    if 'dipole_gradients' in properties.settings:
        if openrsp == '':
            openrsp += '**OPENRSP\n'
            openrsp += '*RESPONSE FUNCTION\n'
        dip_grad += '$PROPERTY\n'
        dip_grad += '.PERTURBATION\n'
        dip_grad += '2\n'
        dip_grad += 'GEO\n'
        dip_grad += 'EL\n'
        dip_grad += '.KN-RULE\n'
        dip_grad += '0\n'
    if 'polarizability_gradients' in properties.settings:
        if openrsp == '':
            openrsp += '**OPENRSP\n'
            openrsp += '*RESPONSE FUNCTION\n'
        pol_grad += '$PROPERTY\n'
        pol_grad += '.PERTURBATION\n'
        pol_grad += '3\n'
        pol_grad += 'GEO\n'
        pol_grad += 'EL\n'
        pol_grad += 'EL\n'
        pol_grad += '.FREQUENCY\n'
        frequencies = properties.settings['polarizability_gradients_frequencies']
        pol_grad += f'{len(frequencies)}\n'
        for i in range(len(frequencies)):
            pol_grad += f'{frequencies[i]}\n'
        pol_grad += '.KN-RULE\n'
        pol_grad += '0\n'
    if openrsp != '' and 'scf_threshold' in qc_method.settings:
        if solver == '':
            solver += '*SOLVER\n'
        solver += '.CONVTHR\n'
        solver += f'{qc_method.settings["scf_threshold"]}\n'
    dal_input += openrsp + hessian + dip_grad + pol_grad + solver

    # *END OF INPUT
    dal_input += '*END OF INPUT\n'
    return dal_input
