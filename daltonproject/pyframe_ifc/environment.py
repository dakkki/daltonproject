#  Copyright (C)  The Dalton Project Developers.
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#  Contact information can be found on our website: https://www.daltonproject.org/

import logging
import os
import shutil
from pathlib import Path
from typing import Optional

from pyframe import MolecularSystem, Project
from pyframe.atoms import Atom, AtomList
from pyframe.fragments import Fragment, FragmentDict

from ..basis import Basis
from ..environment import Environment
from ..molecule import Molecule
from ..program import ComputeSettings
from ..qcmethod import QCMethod
from ..utilities import run

logger = logging.getLogger('daltonproject')


class Embedding:
    """Embedding base class."""

    def __init__(self, input_file: str) -> None:
        if not os.path.isfile(input_file):
            raise FileNotFoundError(f'Input file "{input_file}" was not found.')
        file_path, file_name = os.path.split(input_file)
        file_name, file_ext = os.path.splitext(file_name)
        if not (file_ext.lower() in ['.pdb', '.pqr']):
            raise ValueError(f'File type "{file_ext[1:].upper()}" is not supported. Only PDB or PQR files may be used'
                             f' together with the PyFraME interface.')
        self._environment = None
        self._system: MolecularSystem = MolecularSystem(input_file=input_file)

    def extract_core_region(self,
                            fragment_name: str,
                            fragment_id: int,
                            chain_id: Optional[str] = None,
                            distance: Optional[float] = None,
                            use_center_of_mass: bool = False,
                            protect_molecules: bool = False) -> Molecule:
        """Extract molecule or fragment.

        Args:
            fragment_name: Name of the fragment as given in the input file.
            fragment_id: Sequence number of the fragment as given in the input file.
            chain_id: Chain ID as given in the input file.
            distance: Include fragments that are within the specified distance from fragment with the
                specified fragment_name, fragment_id, and, optionally, chain_id.
            use_center_of_mass: Use the center of mass when computing fragment distances.
            protect_molecules: Keep molecules intact, i.e., do not break bonds. Note that this option will preserve
                proteins and other large molecules.

        Returns:
            Molecule instance where atom names from the input file are used as labels.
        """
        if chain_id is not None:
            identifier = f'{fragment_id}_{chain_id}_{fragment_name}'
        else:
            identifier = f'{fragment_id}_{fragment_name}'
        fragments = self._system.get_fragments_by_identifier(identifiers=[identifier])
        if not fragments:
            raise Exception(f'No fragment with name "{fragment_name}", id "{fragment_id}",'
                            f' and chain id "{chain_id}" was found.')
        if distance is not None:
            fragments += self._system.get_fragments_by_distance(distance=distance,
                                                                reference=fragments,
                                                                use_center_of_mass=use_center_of_mass,
                                                                protect_molecules=protect_molecules)
        return fragment2molecule(sum(fragments.values()))

    def define_environment(self,
                           distance: float,
                           core_region: Molecule,
                           use_center_of_mass: bool = False,
                           protect_molecules: bool = False) -> None:
        """Define the environment composition."""
        core_fragment = molecule2fragment(core_region)
        self._environment = self._system.get_fragments_by_distance(distance=distance,
                                                                   reference=core_fragment,
                                                                   use_center_of_mass=use_center_of_mass,
                                                                   protect_molecules=protect_molecules)


class PolarizableEmbedding(Embedding, Environment):
    """Polarizable embedding (PE) environment model."""

    def __init__(self, input_file: str) -> None:
        Embedding.__init__(self, input_file=input_file)
        Environment.__init__(self, model='PE')

    #         file_path, file_name = os.path.split(input_file)
    #         file_name, file_ext = os.path.splitext(file_name)
    #         self.system: Optional[MolecularSystem] = None
    #         if file_ext.lower() in ['pdb', 'pqr']:
    #             self.system = MolecularSystem(input_file=input_file)
    #         elif file_ext.lower() in ['.pot']:
    #             self.system = pyframe.readers.read_pelib_potential(input_file)
    #         else:
    #             raise ValueError(f'File type "{file_ext}" is not supported.')
    #         self.core_fragments: Optional[FragmentDict] = None

    def get_input_string(self, input_format: str) -> str:
        pot_file = Path(f'./{self._system.name}/{self._system.name}.pot')
        with open(f'{pot_file}') as f:
            input_string = f.read()
        return input_string

    def write_input_files(self, filename: str, input_format: str) -> None:
        pot_file = Path(f'./{self._system.name}/{self._system.name}.pot')
        shutil.copyfile(src=pot_file, dst=f'{filename}.pot')

    def compute_potential_parameters(
        self,
        qc_method: QCMethod = QCMethod('DFT', 'CAM-B3LYP'),
        basis: Basis = Basis('loprop-6-31+G*'),
        compute_settings: ComputeSettings = ComputeSettings()
    ) -> None:
        """Compute embedding potential parameters."""
        method = qc_method.settings['qc_method']
        if method == 'DFT':
            xc_fun = qc_method.settings['xc_functional']
        elif method == 'HF':
            xc_fun = 'dummy'
        else:
            raise ValueError('PyFraME interface currently only allows DFT or HF methods.')
        if not isinstance(basis.basis, str):
            raise ValueError('Basis is currently limited to use same basis for all atoms in PyFraME interface.')
        if (basis.ri or basis.admm) is not None:
            raise ValueError('Basis contains RI or ADMM basis that are currently supported by the PyFraME interface.')
        self._system.add_region(name='daltonproject',
                                fragments=self._environment,
                                use_mfcc=True,
                                mfcc_order=2,
                                use_multipoles=True,
                                multipole_method=method,
                                multipole_xcfun=xc_fun,
                                multipole_basis=basis.basis,
                                use_polarizabilities=True,
                                polarizability_method=method,
                                polarizability_xcfun=xc_fun,
                                polarizability_basis=basis.basis)
        project = Project(work_dir=compute_settings.work_dir,
                          scratch_dir=compute_settings.scratch_dir,
                          node_list=compute_settings.node_list,
                          jobs_per_node=compute_settings.jobs_per_node,
                          mpi_procs_per_job=compute_settings.mpi_num_procs // compute_settings.jobs_per_node,
                          omp_threads_per_job=compute_settings.omp_num_threads,
                          memory_per_job=compute_settings.memory // compute_settings.jobs_per_node,
                          comm_port=compute_settings.comm_port)
        project.create_embedding_potential(self._system)
        project.write_potential(self._system)


class PolarizableDensityEmbedding(Embedding, Environment):
    """Polarizable density embedding (PDE) environment model."""

    def __init__(self, input_file: str) -> None:
        Embedding.__init__(self, input_file=input_file)
        Environment.__init__(self, model='PDE')

    def get_input_string(self, input_format: str) -> str:
        pot_file = Path(f'./{self._system.name}/{self._system.name}.pot')
        h5_file = Path(f'./{self._system.name}/{self._system.name}.h5')
        with open(f'{pot_file}') as f:
            pot_string = f.read()
        h5_string, stderr, return_code = run(f'h5dump {h5_file}')
        if return_code != 0:
            raise Exception(f'Failed to create input files. Error message:\n{stderr}')
        return pot_string + h5_string

    def write_input_files(self, filename: str, input_format: str) -> None:
        pot_file = Path(f'./{self._system.name}/{self._system.name}.pot')
        h5_file = Path(f'./{self._system.name}/{self._system.name}.h5')
        stdout, stderr, return_code = run(f'h5jam -i {h5_file} -u {pot_file} -o {filename}.pot')
        if return_code != 0:
            raise Exception(f'Failed to create input files. Error message:\n{stderr}')
        # shutil.copyfile(src=pot_file, dst=f'{filename}.pot')
        # shutil.copyfile(src=h5_file, dst=f'{filename}.h5')

    def compute_potential_parameters(
        self,
        core_region: Molecule,
        core_basis: Basis,
        qc_method: QCMethod = QCMethod('DFT', 'CAM-B3LYP'),
        basis: Basis = Basis('loprop-6-31+G*'),
        compute_settings: ComputeSettings = ComputeSettings()
    ) -> None:
        """Compute embedding potential parameters."""
        method = qc_method.settings['qc_method']
        if method == 'DFT':
            xc_fun = qc_method.settings['xc_functional']
        elif method == 'HF':
            xc_fun = 'dummy'
        else:
            raise ValueError('PyFraME interface currently only allows DFT or HF methods.')
        if not isinstance(basis.basis, str):
            raise ValueError('Basis is currently limited to use same basis for all atoms in PyFraME interface.')
        if (basis.ri or basis.admm) is not None:
            raise ValueError('Basis contains RI or ADMM basis that are currently supported by the PyFraME interface.')
        if not isinstance(core_basis.basis, str):
            raise ValueError('Basis is currently limited to use same basis for all atoms in PyFraME interface.')
        if (core_basis.ri or core_basis.admm) is not None:
            raise ValueError('Basis contains RI or ADMM basis that are currently supported by the PyFraME interface.')
        self._system.set_core_region(fragments=molecule2fragment(core_region), basis=f'{core_basis.basis}')
        self._system.add_region(name='daltonproject',
                                fragments=self._environment,
                                use_mfcc=True,
                                mfcc_order=2,
                                use_multipoles=False,
                                multipole_method=method,
                                multipole_xcfun=xc_fun,
                                multipole_basis=basis.basis,
                                use_polarizabilities=True,
                                polarizability_method=method,
                                polarizability_xcfun=xc_fun,
                                polarizability_basis=basis.basis,
                                use_fragment_densities=True,
                                fragment_density_method=method,
                                fragment_density_xcfun=xc_fun,
                                fragment_density_basis=basis.basis,
                                use_exchange_repulsion=True,
                                exchange_repulsion_method=method,
                                exchange_repulsion_xcfun=xc_fun,
                                exchange_repulsion_basis=basis.basis)
        project = Project(work_dir=compute_settings.work_dir,
                          scratch_dir=compute_settings.scratch_dir,
                          node_list=compute_settings.node_list,
                          jobs_per_node=compute_settings.jobs_per_node,
                          mpi_procs_per_job=compute_settings.mpi_num_procs // compute_settings.jobs_per_node,
                          omp_threads_per_job=compute_settings.omp_num_threads,
                          memory_per_job=compute_settings.memory // compute_settings.jobs_per_node,
                          comm_port=compute_settings.comm_port)
        project.create_embedding_potential(self._system)
        project.write_potential(self._system)


def fragment2molecule(fragment: Fragment) -> Molecule:
    """Convert PyFraME fragment to Dalton Project molecule."""
    charge = fragment.charge
    atoms = ''
    for atom in fragment.atoms:
        atoms += f'{atom.element} {atom.coordinate[0]} {atom.coordinate[1]} {atom.coordinate[2]} {atom.name}\n'
    # The slice removes the last new-line character
    return Molecule(atoms=atoms[:-1], charge=charge)


def molecule2fragment(molecule: Molecule) -> FragmentDict:
    """Convert Dalton Project molecule to PyFraME fragment."""
    atoms = AtomList()
    for i, (element, coordinate, label) in enumerate(zip(molecule.elements, molecule.coordinates,
                                                         molecule.labels)):
        atoms.append(
            Atom(name=label,
                 number=i,
                 element=element,
                 coordinate=coordinate,
                 charge=molecule.charge / molecule.num_atoms))
    fragment = FragmentDict({'dummy': Fragment(name='dummy', identifier='dummy', atoms=atoms)})
    return fragment
