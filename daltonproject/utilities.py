#  Dalton Project: A Python platform for molecular- and electronic-structure
#  simulations of complex systems
#
#  Copyright (C)  The Dalton Project Developers.
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#  Contact information can be found on our website: https://www.daltonproject.org/

import contextlib
import hashlib
import os
import platform
import queue
import socket
import string
import subprocess as sp
import sys
from multiprocessing.managers import BaseManager
from typing import Generator, List, Tuple

import parse
import psutil


@contextlib.contextmanager
def chdir(path: str) -> Generator:
    """Change directory with-statement context."""
    old_cwd = os.getcwd()
    os.chdir(path)
    try:
        yield
    finally:
        os.chdir(old_cwd)


def unique_filename(input_strings: List[str]) -> str:
    """Create a unique filename based on a list of input strings.

    Args:
        input_strings: List of input strings.

    Returns:
        Unique filename.
    """
    if not isinstance(input_strings, list):
        raise TypeError(f'Expected list, {type(input_strings).__name__} found.')
    for i, input_string in enumerate(input_strings):
        if not isinstance(input_string, str):
            raise TypeError(f'List item {i}: expected str instance, {type(input_string).__name__} found.')
    return hashlib.sha1(''.join(input_strings).encode('utf-8')).hexdigest()


def num_numa_nodes() -> int:
    """Number of NUMA nodes.

    Works on most Linux distributions but may fail on MacOS and give wrong
    value on Windows.
    """
    if platform.system() == 'Windows':
        command = 'wmic cpu list brief'
        stdout, stderr, return_code = run(command)
        if return_code != 0:
            raise Exception(f'Failed attempt to run {command}. Error message:\n{stderr}')
        num_nodes = len(stdout.split()) - 1
    else:
        command = 'lscpu'
        stdout, stderr, return_code = run(command)
        if return_code != 0:
            raise Exception(f'Failed attempt to run {command}. Error message:\n{stderr}')
        result = parse.search('NUMA node(s): {num_numa_nodes:>d}', stdout)
        if result is None:
            raise Exception('Unable to detect number of NUMA nodes.')
        num_nodes = result.named['num_numa_nodes']
    return num_nodes


def num_cpu_cores() -> int:
    """Number of CPU cores.

    Works on most Linux distributions but may fail on MacOS and give wrong core
    count on Windows.
    """
    if platform.system() == 'Windows':
        num_cores = psutil.cpu_count(logical=False)
    else:
        command = 'lscpu'
        stdout, stderr, return_code = run(command)
        if return_code != 0:
            raise Exception(f'Failed attempt to run {command}. Error message:\n{stderr}')
        result = parse.search('Core(s) per socket: {num_cores_per_socket:>d}', stdout)
        if result is None:
            raise Exception('Unable to detect number of CPU cores.')
        num_cores_per_socket = result.named['num_cores_per_socket']
        result = parse.search('Socket(s): {num_sockets:>d}', stdout)
        if result is None:
            raise Exception('Unable to detect number of CPU cores.')
        num_sockets = result.named['num_sockets']
        num_cores = num_cores_per_socket * num_sockets
    return num_cores


def run(command: str) -> Tuple[str, str, int]:
    """Run a command.

    Args:
        command: Commands with arguments given as a list of strings.

    Returns:
        Standard output, standard error, and return code.
    """
    if not isinstance(command, str):
        raise TypeError(f'Expected str, {type(command).__name__} found.')
    result = sp.run(command, stdout=sp.PIPE, stderr=sp.PIPE, encoding='utf-8', shell=True)
    return result.stdout, result.stderr, result.returncode


def job_farm(commands: List[str], node_list: List[str], jobs_per_node: int,
             comm_port: int) -> List[Tuple[str, str, int]]:
    """Run a series of jobs in parallel.

    Args:
        commands: List of commands to run.
        node_list: List of nodes on which the commands are run.
        jobs_per_node: Number of jobs per node.
        comm_port: Port used for server-client communication.
    """
    auth_key = os.urandom(8)
    manager = create_server_manager(comm_port, auth_key)
    manager.start()
    job_queue = manager.get_job_queue()
    result_queue = manager.get_result_queue()
    script_name = 'dalton_project_client.py'
    start_clients(script_name, node_list, jobs_per_node, comm_port, auth_key)
    environment_variables = []
    if 'PATH' in os.environ:
        environment_variables.append(f'PATH={os.environ["PATH"]}')
    if 'LD_LIBRARY_PATH' in os.environ:
        environment_variables.append(f'LD_LIBRARY_PATH={os.environ["LD_LIBRARY_PATH"]}')
    if 'DP_VARIABLES' in os.environ:
        environment_variables.append(f'{os.environ["DP_VARIABLES"]}')
    for command in commands:
        full_command = f'env {" ".join(environment_variables)} bash -l -c "cd {os.getcwd()}; {command}"'
        job_queue.put(full_command)
    job_queue.join()
    num_jobs = len(commands)
    num_nodes = len(node_list)
    for _ in range(jobs_per_node * num_nodes):
        job_queue.put(None)
    results = []
    while num_jobs > 0:
        result = result_queue.get()
        results.append(result)
        result_queue.task_done()
        num_jobs -= 1
    manager.shutdown()
    destroy_clients(script_name)
    return results


def create_server_manager(comm_port: int, auth_key: bytes):
    """Create a queue manager."""

    class QueueManager(BaseManager):
        """Queue manager."""
        pass

    job_queue: queue.Queue = queue.Queue()
    result_queue: queue.Queue = queue.Queue()
    QueueManager.register('get_job_queue', callable=lambda: job_queue)
    QueueManager.register('get_result_queue', callable=lambda: result_queue)
    manager = QueueManager(address=('', comm_port), authkey=auth_key)
    return manager


def start_clients(script_name: str, node_list: List[str], jobs_per_node: int, comm_port: int,
                  auth_key: bytes) -> None:
    """Start a client on each node.

    Args:
        script_name: Name of the worker script.
        node_list: List of nodes.
        jobs_per_node: Number of jobs per node.
        comm_port: Port used to communicate with the server.
        auth_key: Authentication used to validate connection to server.
    """
    create_client_script(script_name, jobs_per_node, comm_port, auth_key)
    for node in node_list:
        remote_command = 'env'
        if 'PYTHONPATH' in os.environ:
            remote_command += f' PYTHONPATH={os.environ["PYTHONPATH"]}'
        remote_command += ' bash -l -c'
        remote_command += f' "cd {os.getcwd()}; nohup {sys.executable} {script_name} &> {node}.out &"'
        command = f'ssh {node} \'{remote_command}\''
        _, stderr, return_code = run(command)
        if return_code != 0:
            raise Exception(f'Unable to start client on {node}. Error message:\n{stderr}')


def destroy_clients(script_name: str) -> None:
    """Shutdown and cleanup after clients."""
    os.remove(script_name)


def create_client_script(script_name: str, jobs_per_node: int, comm_port: int, auth_key: bytes) -> None:
    """Create client script.

    Args:
        script_name: Name of the client script.
        jobs_per_node: Number of jobs per node.
        comm_port: Port used for server-client communication.
        auth_key: Authentication key used to validate connection to server.
    """
    client_script = string.Template("""import multiprocessing as mp
from multiprocessing.managers import BaseManager
import queue
import subprocess as sp


class QueueManager(BaseManager):
    pass


QueueManager.register('get_job_queue')
QueueManager.register('get_result_queue')


def run(command):
    result = sp.run(command, stdout=sp.PIPE, stderr=sp.PIPE, encoding='utf-8', shell=True)
    return result.stdout, result.stderr, result.returncode


def start_client():
    client = create_client_manager()
    client.connect()
    jobs = []
    for i in range($jobs_per_node):
        job = mp.Process(target=client_worker, args=(client.get_job_queue(), client.get_result_queue()))
        job.start()
        jobs.append(job)
    for job in jobs:
        job.join()


def create_client_manager():
    class QueueManager(BaseManager):
        pass
    QueueManager.register('get_job_queue')
    QueueManager.register('get_result_queue')
    manager = QueueManager(address=('$hostname', $comm_port), authkey=$auth_key)
    return manager


def client_worker(job_queue, result_queue):
    while True:
        command = job_queue.get()
        if command is None:
            job_queue.task_done()
            break
        result = run(command)
        job_queue.task_done()
        result_queue.put(result)
    result_queue.join()


if __name__ == '__main__':
    start_client()
""").substitute(jobs_per_node=jobs_per_node, hostname=socket.gethostname(), comm_port=comm_port, auth_key=auth_key)
    with open(script_name, 'w') as f:
        f.write(client_script)
