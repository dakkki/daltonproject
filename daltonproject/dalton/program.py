"""Interface to the Dalton program that sets up and runs calculations through subprocess."""
#  Copyright (C)  The Dalton Project Developers.
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#  Contact information can be found on our website: https://www.daltonproject.org/
import logging
import os
import shutil
import warnings
from collections import defaultdict, namedtuple
from typing import Dict, List, Optional, Tuple, Union

import numpy as np
import parse
from qcelemental import periodictable

from ..basis import Basis, get_atom_basis
from ..environment import Environment
from ..molecule import Molecule
from ..program import ComputeSettings, Program
from ..property import Property
from ..qcmethod import QCMethod
from ..symmetry import SUPERGROUPS
from ..utilities import chdir, job_farm, num_cpu_cores, run, unique_filename
from .output_parser import OutputParser

logger = logging.getLogger('daltonproject')


class Dalton(Program):
    """Interface to the Dalton program."""

    program_name: str = 'dalton'

    @classmethod
    def compute(cls,
                molecule: Molecule,
                basis: Basis,
                qc_method: QCMethod,
                properties: Property,
                environment: Optional[Environment] = None,
                compute_settings: Optional[ComputeSettings] = None,
                filename: Optional[str] = None,
                force_recompute: Optional[bool] = False) -> OutputParser:
        """Run a calculation using the Dalton program.

        Args:
            molecule: Molecule on which a calculations is performed. This can
                also be an atom, a fragment, or any collection of atoms.
            basis: Basis set to use in the calculation.
            qc_method: Quantum chemistry method, e.g., HF, DFT, or CC, and
                associated settings.
            properties: Properties of molecule to be calculated, geometry
                optimization, excitation energies, etc.
            environment: TODO: Environment ...
            compute_settings: Settings for the calculation, e.g., number of MPI
                processes and OpenMP threads, work and scratch directory, etc.
            filename: Optional user-specified filename that will be used for
                input and output files. If not specified a name will be
                generated as a hash of the input.
            force_recompute: Recompute even if the output files already exist.

        Returns:
            OutputParser instance that contains the filename of the output
            produced in the calculation and can be used to extract results from
            the output.
        """
        Program._validate_compute_args(molecule, basis, qc_method, properties, environment, compute_settings,
                                       filename, force_recompute)
        mol_input = molecule_input(molecule, basis)
        dal_input = dalton_input(qc_method, properties, environment)
        env_input = ''
        if environment:
            env_input = environment.get_input_string(input_format='dalton')
        if compute_settings is None:
            if '.CC' in dal_input:
                mpi_num_procs = 1
            else:
                mpi_num_procs = num_cpu_cores()
            compute_settings = ComputeSettings(mpi_num_procs=mpi_num_procs)
        if not filename:
            input_list = [dal_input, mol_input, env_input]
            filename = unique_filename(input_list)
        filepath = os.path.join(compute_settings.work_dir, filename)
        if os.path.isfile(f'{filepath}.out') and os.path.isfile(f'{filepath}.tar.gz') and not force_recompute:
            logger.info('Output files already exist and will be reused.')
            return OutputParser(filepath)
        if shutil.which(cmd=cls.program_name) is None:
            raise FileNotFoundError(f'The {cls.program_name} script was not found or is not executable.')
        if 'DALTON_LAUNCHER' in os.environ:
            dalton_launcher = os.environ['DALTON_LAUNCHER']
        elif compute_settings.launcher is not None:
            dalton_launcher = compute_settings.launcher
        else:
            dalton_launcher = f'env OMP_NUM_THREADS={compute_settings.omp_num_threads}'
            if compute_settings.mpi_num_procs > 1:
                dalton_launcher += f' {compute_settings.mpi_command} {compute_settings.mpi_num_procs}'
        with chdir(compute_settings.work_dir):
            with open(f'{filename}.mol', 'w') as mol_file:
                mol_file.write(mol_input)
            with open(f'{filename}.dal', 'w') as dal_file:
                dal_file.write(dal_input)
            basis.write()
            if environment:
                environment.write_input_files(filename=filename, input_format='dalton')
            command = (f'env DALTON_LAUNCHER="{dalton_launcher}"'
                       f' {cls.program_name}'
                       f' -nobackup'
                       f' -t {compute_settings.scratch_dir}'
                       f' -mb {compute_settings.memory // compute_settings.mpi_num_procs}'
                       f' -mol {filename}.mol'
                       f' -dal {filename}.dal'
                       f' -o {filename}.out')
            if environment:
                if environment.model in ('PE', 'PDE'):
                    command += f' -pot {filename}.pot'
            stdout, stderr, return_code = run(command)
        if return_code != 0:
            result = parse.search('Reason: {}\n', stderr)
            if result is None:
                error_message = stderr
            else:
                error_message = result[0]
            raise Exception(f'Dalton exited with non-zero return code. Error message:\n{error_message}')
        return OutputParser(filepath)

    @classmethod
    def compute_farm(cls,
                     molecules: List[Molecule],
                     basis: Basis,
                     qc_method: QCMethod,
                     properties: Property,
                     compute_settings: ComputeSettings = None,
                     filenames: List[str] = None,
                     force_recompute: bool = False) -> List[OutputParser]:
        """Run a series of calculations using the Dalton program.

        Args:
            molecules: List of molecules on which calculations are performed.
            basis: Basis set to use in the calculations.
            qc_method: Quantum chemistry method, e.g., HF, DFT, or CC, and
                associated settings used for all calculations.
            properties: Properties of molecule to be calculated, geometry
                optimization, excitation energies, etc., computed for all
                molecules.
            compute_settings: Settings for the calculations, e.g., number of
                MPI processes and OpenMP threads, work and scratch directory,
                and more.
            filenames: Optional list of user-specified filenames that will be
                used for input and output files. If not specified names will be
                generated as a hash of the individual inputs.
            force_recompute: Recompute even if the output files already exist.

        Returns:
            List of OutputParser instances each of which contains the filename
            of the output produced in the calculation and can be used to
            extract results from the output.
        """
        mol_inputs = [molecule_input(molecule, basis) for molecule in molecules]
        dal_input = dalton_input(qc_method, properties)
        if compute_settings is None:
            if '.CC' in dal_input:
                mpi_num_procs = 1
            else:
                mpi_num_procs = num_cpu_cores()
            compute_settings = ComputeSettings(mpi_num_procs=mpi_num_procs)
        if not filenames:
            input_lists = [[dal_input, mol_input] for mol_input in mol_inputs]
            filenames = [unique_filename(input_list) for input_list in input_lists]
        if 'DALTON_LAUNCHER' in os.environ:
            dalton_launcher = os.environ['DALTON_LAUNCHER']
        elif compute_settings.launcher is not None:
            dalton_launcher = compute_settings.launcher
        else:
            dalton_launcher = f'env OMP_NUM_THREADS={compute_settings.omp_num_threads}'
            if compute_settings.mpi_num_procs > 1:
                mpi_procs_per_job = compute_settings.mpi_num_procs // (compute_settings.jobs_per_node
                                                                       * compute_settings.num_nodes)
                dalton_launcher += f' {compute_settings.mpi_command} {mpi_procs_per_job}'
        os.environ['DP_VARIABLES'] = f'DALTON_LAUNCHER="{dalton_launcher}"'
        with chdir(compute_settings.work_dir):
            commands = []
            output_parsers = []
            for filename, mol_input in zip(filenames, mol_inputs):
                output_parsers.append(OutputParser(os.path.join(compute_settings.work_dir, filename)))
                if os.path.isfile(f'{filename}.out') and os.path.isfile(f'{filename}.tar.gz') and not force_recompute:
                    continue
                if shutil.which(cmd=cls.program_name) is None:
                    raise FileNotFoundError(f'The {cls.program_name} script was not found or is not executable.')
                with open(f'{filename}.mol', 'w') as mol_file:
                    mol_file.write(mol_input)
                with open(f'{filename}.dal', 'w') as dal_file:
                    dal_file.write(dal_input)
                basis.write()
                mpi_procs_per_node = compute_settings.mpi_num_procs // len(compute_settings.node_list)
                command = (f'{cls.program_name}'
                           f' -nobackup'
                           f' -t {compute_settings.scratch_dir}'
                           f' -mb {compute_settings.memory // mpi_procs_per_node}'
                           f' -mol {filename}.mol'
                           f' -dal {filename}.dal'
                           f' -o {filename}.out')
                commands.append(command)
            results = job_farm(commands, compute_settings.node_list, compute_settings.jobs_per_node,
                               compute_settings.comm_port)
            for stdout, stderr, return_code in results:
                if return_code != 0:
                    result = parse.search('Reason: {}\n', stderr)
                    if result is None:
                        error_message = stderr
                    else:
                        error_message = result[0]
                    warnings.warn(f'Dalton exited with non-zero return code. Error message:\n{error_message}')
        os.environ.pop('DP_VARIABLES')
        return output_parsers


def molecule_input(molecule: Molecule, basis: Basis) -> str:
    """Generate Dalton molecule file input as a string.

    Args:
      molecule: Molecule object.
      basis: Basis object.
    """
    atom_bases = get_atom_basis(basis.basis, molecule.num_atoms, molecule.labels)
    if molecule.point_group in SUPERGROUPS['C1']:
        mol_input = molecule_input_nosym(molecule, atom_bases)
    else:
        mol_input = molecule_input_sym(molecule, atom_bases)
    return mol_input


def molecule_input_nosym(molecule: Molecule, atom_bases: List[str]) -> str:
    """Generate Dalton molecule input without using molecular symmetry.

    Args:
        molecule: Molecule class object.
        atom_bases: Basis sets used for the different atoms.

    Returns:
        String that contains Dalton molecule input.
    """
    atom_types = 0
    coordinate_groups = []
    coordinate_group = []
    element_group = []
    basis_group = []
    previous_element = None
    previous_basis = None
    for element, coordinate, atom_basis in zip(molecule.elements, molecule.coordinates, atom_bases):
        if element != previous_element or atom_basis != previous_basis:
            coordinate_group = [coordinate]
            coordinate_groups.append(coordinate_group)
            element_group.append(element)
            basis_group.append(atom_basis)
            atom_types += 1
        else:
            coordinate_group.append(coordinate)
        previous_element = element
        previous_basis = atom_basis
    mol_input = 'ATOMBASIS\n'
    mol_input += 'Generated by Dalton Project\n'
    mol_input += '\n'
    mol_input += f'Atomtypes={atom_types} Charge={molecule.charge} Nosymmetry Angstrom\n'
    for basis, element, coordinate_group in zip(basis_group, element_group, coordinate_groups):
        mol_input += f'Charge={periodictable.to_atomic_number(element):.1f} Atoms={len(coordinate_group)}' \
                     f' Basis={basis}\n'
        for coordinate in coordinate_group:
            mol_input += f'{element:2} {coordinate[0]:12.6f} {coordinate[1]:12.6f} {coordinate[2]:12.6f}\n'
    return mol_input


def molecule_input_sym(molecule: Molecule, atom_bases: List[str]) -> str:
    """Generate Dalton molecule input using molecular symmetry.

    Args:
        molecule: Molecule class object.
        atom_bases: Basis sets used for the different atoms.

    Returns:
        String that contains Dalton molecule input.
    """
    if molecule.point_group in SUPERGROUPS['Cs']:
        xyz1, xyz2, xyz3 = np.array([1, 1, -1]), np.array([1, 1, 1]), np.array([1, 1, 1])
        generator = '1 Z'
    elif molecule.point_group in SUPERGROUPS['C3v']:
        xyz1, xyz2, xyz3 = np.array([-1, 1, 1]), np.array([1, 1, 1]), np.array([1, 1, 1])
        generator = '1 X'
    elif molecule.point_group in SUPERGROUPS['C2']:
        xyz1, xyz2, xyz3 = np.array([-1, -1, 1]), np.array([1, 1, 1]), np.array([1, 1, 1])
        generator = '1 XY'
    elif molecule.point_group in SUPERGROUPS['D3']:
        xyz1, xyz2, xyz3 = np.array([1, -1, -1]), np.array([1, 1, 1]), np.array([1, 1, 1])
        generator = '1 YZ'
    elif molecule.point_group in SUPERGROUPS['Ci']:
        generator = '1 XYZ'
        xyz1, xyz2, xyz3 = np.array([-1, -1, -1]), np.array([1, 1, 1]), np.array([1, 1, 1])
    elif molecule.point_group in SUPERGROUPS['C2v']:
        xyz1, xyz2, xyz3 = np.array([-1, 1, 1]), np.array([1, -1, 1]), np.array([1, 1, 1])
        generator = '2 X Y'
    elif molecule.point_group in SUPERGROUPS['D3h']:
        xyz1, xyz2, xyz3 = np.array([1, 1, -1]), np.array([1, -1, 1]), np.array([1, 1, 1])
        generator = '2 Z Y'
    elif molecule.point_group in SUPERGROUPS['C2h']:
        xyz1, xyz2, xyz3 = np.array([1, 1, -1]), np.array([-1, -1, 1]), np.array([1, 1, 1])
        generator = '2 Z XY'
    elif molecule.point_group in SUPERGROUPS['D3d']:
        xyz1, xyz2, xyz3 = np.array([-1, 1, 1]), np.array([1, -1, -1]), np.array([1, 1, 1])
        generator = '2 X YZ'
    elif molecule.point_group in SUPERGROUPS['D2']:
        xyz1, xyz2, xyz3 = np.array([-1, 1, -1]), np.array([1, -1, -1]), np.array([1, 1, 1])
        generator = '2 XZ YZ'
    elif molecule.point_group in SUPERGROUPS['D2h']:
        xyz1, xyz2, xyz3 = np.array([-1, 1, 1]), np.array([1, -1, 1]), np.array([1, 1, -1])
        generator = '3 X Y Z'
    else:
        raise Exception(f'Unknown point group: {molecule.point_group}')
    # It is required for the molecule to be oriented correctly in order to make a Dalton generator input.
    molecule.coordinates = molecule.symmetry.rotate_molecule_pseudo_convention()
    temp_coords = list(molecule.coordinates)
    temp_elements = list(molecule.elements)
    temp_basis = atom_bases
    unique_elements = []
    unique_coords = []
    unique_basis = []
    for _ in range(len(temp_elements)):
        coords = temp_coords[0]
        unique_elements.append(temp_elements[0])
        unique_coords.append(list(temp_coords[0]))
        unique_basis.append(temp_basis[0])
        for i in range(len(temp_elements) - 1, -1, -1):
            coords_arr = temp_coords[i]
            if np.all(np.abs(coords - coords_arr) < molecule.symmetry_threshold):
                temp_coords.pop(i)
                temp_elements.pop(i)
                temp_basis.pop(i)
            elif np.all(np.abs(xyz1*coords - coords_arr) < molecule.symmetry_threshold):
                temp_coords.pop(i)
                temp_elements.pop(i)
                temp_basis.pop(i)
            elif np.all(np.abs(xyz2*coords - coords_arr) < molecule.symmetry_threshold):
                temp_coords.pop(i)
                temp_elements.pop(i)
                temp_basis.pop(i)
            elif np.all(np.abs(xyz1*xyz2*coords - coords_arr) < molecule.symmetry_threshold):
                temp_coords.pop(i)
                temp_elements.pop(i)
                temp_basis.pop(i)
            elif np.all(np.abs(xyz3*coords - coords_arr) < molecule.symmetry_threshold):
                temp_coords.pop(i)
                temp_elements.pop(i)
                temp_basis.pop(i)
            elif np.all(np.abs(xyz1*xyz3*coords - coords_arr) < molecule.symmetry_threshold):
                temp_coords.pop(i)
                temp_elements.pop(i)
                temp_basis.pop(i)
            elif np.all(np.abs(xyz2*xyz3*coords - coords_arr) < molecule.symmetry_threshold):
                temp_coords.pop(i)
                temp_elements.pop(i)
                temp_basis.pop(i)
            elif np.all(np.abs(xyz1*xyz2*xyz3*coords - coords_arr) < molecule.symmetry_threshold):
                temp_coords.pop(i)
                temp_elements.pop(i)
                temp_basis.pop(i)
        if len(temp_elements) == 0:
            break
    unique_elements, unique_coords, unique_basis = zip(*sorted(zip(unique_elements, unique_coords, unique_basis)))
    atom_types = 0
    coordinate_groups = []
    coordinate_group = []
    element_group = []
    basis_group = []
    previous_element = None
    previous_basis = None
    for element, coordinate, atom_basis in zip(unique_elements, unique_coords, unique_basis):
        if element != previous_element or atom_basis != previous_basis:
            coordinate_group = [coordinate]
            coordinate_groups.append(coordinate_group)
            element_group.append(element)
            basis_group.append(atom_basis)
            atom_types += 1
        else:
            coordinate_group.append(coordinate)
        previous_element = element
        previous_basis = atom_basis
    mol_input = 'ATOMBASIS\n'
    mol_input += 'Generated by Dalton Project\n'
    mol_input += '\n'
    mol_input += f'Atomtypes={atom_types} Charge={molecule.charge} Generators={generator} Angstrom\n'
    for basis, element, coordinate_group in zip(basis_group, element_group, coordinate_groups):
        mol_input += f'Charge={periodictable.to_atomic_number(element):.1f} Atoms={len(coordinate_group)}' \
                     f' Basis={basis}\n'
        for coordinate in coordinate_group:
            mol_input += f'{element:2} {coordinate[0]:12.6f} {coordinate[1]:12.6f} {coordinate[2]:12.6f}\n'
    return mol_input


def write_molecule_input(molecule: Molecule, basis: Basis, filename: str) -> None:
    """Write Dalton molecule input to file.

    The string for the file is generated in molecule_input, this is to ease
    testing of correct Dalton input generation.

    Args:
        molecule: Molecule object.
        basis: Basis object.
        filename: Name of Dalton molecule file without file extension.
    """
    with open(f'{filename}.mol', 'w') as mol_file:
        mol_file.write(molecule_input(molecule, basis))


def dalton_input(qc_method: QCMethod, properties: Property, environment: Optional[Environment] = None) -> str:
    """Generate Dalton input as a string.

    Args:
        qc_method: QCMethod object.
        properties: Property object.
        environment: Environment object.

    Returns:
        String of Dalton input.
    """

    def input_tree():
        """Create a recursive defaultdict structure.

        When accessing an unbound value, a nested dictionary is created,
        with the final value initialized to an empty defaultdict(input_tree).

        Returns:
            Recursive defaultdict.
        """
        return defaultdict(input_tree)

    # yapf: disable
    def scf_input(qc_method, sections):
        """Add standard SCF settings.

        Args:
            qc_method: QCMethod object.

        Returns:
            Sections of settings.
        """
        if 'scf_threshold' in qc_method.settings:
            sections['**WAVE FUNCTIONS']['*SCF INPUT']['.THRESH'] = qc_method.settings['scf_threshold']
        return sections

    def mcscf_input(qc_method, sections):
        """Add standard MCSCF settings.

        Args:
            qc_method: QCMethod object.

        Returns:
            Sections of settings.
        """
        if 'orbitals' not in qc_method.settings:
            raise AttributeError('No orbitals were supplied for the CAS calculation.')
        sections['**WAVE FUNCTIONS']['*CONFIGURATION INPUT']['.INACTIVE'] = f"{' '.join(map(str, qc_method.settings['num_inactive_orbitals']))}"  # noqa: E501
        sections['**WAVE FUNCTIONS']['*CONFIGURATION INPUT']['.CAS SPACE'] = f"{' '.join(map(str, qc_method.settings['num_cas_orbitals']))}"  # noqa: E501
        sections['**WAVE FUNCTIONS']['*CONFIGURATION INPUT']['.ELECTRONS'] = f"{qc_method.settings['num_active_electrons']}"  # noqa: E501
        if 'state_number' in qc_method.settings:
            sections['**WAVE FUNCTIONS']['*OPTIMIZATION']['.STATE'] = f"{qc_method.settings['state_number']}"
        else:
            sections['**WAVE FUNCTIONS']['*OPTIMIZATION']['.STATE'] = '1'
        if 'state_symmetry' in qc_method.settings:
            sections['**WAVE FUNCTIONS']['*CONFIGURATION INPUT']['.SYMMETRY'] = f"{qc_method.settings['state_symmetry']}"  # noqa: E501
        else:
            sections['**WAVE FUNCTIONS']['*CONFIGURATION INPUT']['.SYMMETRY'] = '1'
        sections['**WAVE FUNCTIONS']['*OPTIMIZATION']['.MAX MACRO ITERATIONS'] = '100'
        sections['**WAVE FUNCTIONS']['*OPTIMIZATION']['.MAX MICRO ITERATIONS'] = '100'
        if 'scf_threshold' in qc_method.settings:
            sections['**WAVE FUNCTIONS']['*OPTIMIZATION']['.THRESH'] = qc_method.settings['scf_threshold']
        return sections

    def ci_input(qc_method, sections):
        """Add standard CI settings.

        Args:
            qc_method: QCMethod object.

        Returns:
            Sections of settings.
        """
        if 'orbitals' not in qc_method.settings:
            raise AttributeError('No orbitals were supplied for the CAS calculation.')
        sections['**WAVE FUNCTIONS']['*CONFIGURATION INPUT']['.INACTIVE'] = f"{' '.join(map(str, qc_method.settings['num_inactive_orbitals']))}"  # noqa: E501
        sections['**WAVE FUNCTIONS']['*CONFIGURATION INPUT']['.CAS SPACE'] = f"{' '.join(map(str, qc_method.settings['num_cas_orbitals']))}"  # noqa: E501
        sections['**WAVE FUNCTIONS']['*CONFIGURATION INPUT']['.ELECTRONS'] = f"{qc_method.settings['num_active_electrons']}"  # noqa: E501
        if 'state_number' in qc_method.settings:
            sections['**WAVE FUNCTIONS']['*CONFIGURATION INPUT']['.STATE'] = f"{qc_method.settings['state_number']}"
        else:
            sections['**WAVE FUNCTIONS']['*CONFIGURATION INPUT']['.STATE'] = '1'
        if 'state_symmetry' in qc_method.settings:
            sections['**WAVE FUNCTIONS']['*CONFIGURATION INPUT']['.SYMMETRY'] = f"{qc_method.settings['state_symmetry']}"  # noqa: E501
        else:
            sections['**WAVE FUNCTIONS']['*CONFIGURATION INPUT']['.SYMMETRY'] = '1'
        return sections

    def srdft_input(qc_method, sections):
        """Add standard srDFT settings.

        Args:
            qc_method: QCMethod object.

        Returns:
            Sections of settings.
        """
        sections['**INTEGRALS']['*TWOINT']['.DOSRINTEGRALS']
        sections['**INTEGRALS']['*TWOINT']['.ERF'] = f"{qc_method.settings['range_separation_parameter']}"
        sections['**WAVE FUNCTIONS']['.SRFUN'] = f"{qc_method.settings['xc_functional']}"
        if 'exact_exchange' in qc_method.settings:
            sections['**WAVE FUNCTIONS']['.HFXFAC'] = qc_method.settings['exact_exchange']
        return sections
    # yapf: enable

    sections = input_tree()

    # yapf: disable
    sections['**DALTON INPUT']['.DIRECT']
    if environment is not None:
        if environment.model in ('PE', 'PDE'):
            sections['**DALTON INPUT']['.PELIB']
            sections['*PELIB']['.EEF']
            if environment.model == 'PDE':
                sections['*PELIB']['.PDE']
    if 'orbitals' in qc_method.settings:
        punched_orbitals, deleted_orbitals = punch_orbitals(qc_method.settings['orbitals'])
        sections['**MOLORB'] = punched_orbitals
    if qc_method.settings['qc_method'] == 'HF':
        sections['**WAVE FUNCTIONS']['.HF']
        sections = scf_input(qc_method, sections)
    elif qc_method.settings['qc_method'] == 'DFT':
        sections['**WAVE FUNCTIONS']['.DFT'] = qc_method.settings['xc_functional']
        sections = scf_input(qc_method, sections)
        if 'exact_exchange' in qc_method.settings:
            raise NotImplementedError('Exact exchange cannot be changed for Dalton through Dalton Project')
    elif qc_method.settings['qc_method'] == 'MP2':
        sections['**WAVE FUNCTIONS']['.HF']
        sections['**WAVE FUNCTIONS']['.MP2']
        sections['**DALTON INPUT']['.FCKTRA']
        sections = scf_input(qc_method, sections)
    if qc_method.settings['qc_method'][:2] == 'CC':
        sections['**WAVE FUNCTIONS']['.CC']
        sections['**WAVE FUNCTIONS']['*CC INPUT'][f'.{qc_method.settings["qc_method"]}']
        sections['**WAVE FUNCTIONS']['*CC INPUT']['.MAXRED'] = 300
    elif qc_method.settings['qc_method'] == 'CASSCF':
        sections['**WAVE FUNCTIONS']['.MCSCF']
        sections['**DALTON INPUT']['.FCKTRA']
        sections = mcscf_input(qc_method, sections)
    elif qc_method.settings['qc_method'] == 'CASCI':
        sections['**WAVE FUNCTIONS']['.CI']
        sections['**DALTON INPUT']['.FCKTRA']
        sections = ci_input(qc_method, sections)
    elif qc_method.settings['qc_method'] == 'HFSRDFT':
        sections['**WAVE FUNCTIONS']['.HFSRDFT']
        sections = scf_input(qc_method, sections)
        sections = srdft_input(qc_method, sections)
    elif qc_method.settings['qc_method'] == 'MP2SRDFT':
        sections['**WAVE FUNCTIONS']['.HFSRDFT']
        sections['**WAVE FUNCTIONS']['.MP2']
        sections['**DALTON INPUT']['.FCKTRA']
        sections = scf_input(qc_method, sections)
        sections = srdft_input(qc_method, sections)
    elif qc_method.settings['qc_method'] == 'CASSRDFT':
        sections['**WAVE FUNCTIONS']['.MCSRDFT']
        sections = srdft_input(qc_method, sections)
        sections = mcscf_input(qc_method, sections)
    elif qc_method.settings['qc_method'] == 'CASCISRDFT':
        sections['**WAVE FUNCTIONS']['.CISRDFT']
        sections = srdft_input(qc_method, sections)
        sections = ci_input(qc_method, sections)
    if 'orbitals' in qc_method.settings:
        punched_orbitals, deleted_orbitals = punch_orbitals(qc_method.settings['orbitals'])
        sections['**WAVE FUNCTIONS']['*ORBITAL INPUT']['.MOSTART'] = 'FORM18'
        if np.sum(deleted_orbitals) != 0:
            sections['**WAVE FUNCTIONS']['*ORBITAL INPUT']['.DELETE'] = deleted_orbitals
        sections['**WAVE FUNCTIONS']['*ORBITAL INPUT']['.PUNCHINORBITALS']
    if 'energy' in properties.settings:
        sections['**DALTON INPUT']['.RUN WAVE FUNCTION']
    if 'dipole' in properties.settings:
        sections['**DALTON INPUT']['.RUN PROPERTIES']
        sections['**PROPERTIES']
    if 'polarizability' in properties.settings:
        sections['**DALTON INPUT']['.RUN RESPONSE']
        sections['**RESPONSE']['*LINEAR']['.DIPLEN']
    if 'first_hyperpolarizability' in properties.settings:
        sections['**DALTON INPUT']['.RUN RESPONSE']
        sections['**RESPONSE']['*QUADRATIC']['.DIPLEN']
    if 'gradients' in properties.settings:
        sections['**DALTON INPUT']['.RUN PROPERTIES']
        sections['**PROPERTIES']['.MOLGRA']
    if 'geometry_optimization' in properties.settings:
        sections['**DALTON INPUT']['.OPTIMIZE']
    if 'excitation_energies' in properties.settings and 'excitation_energies_tpa' not in properties.settings:
        if 'CC' in qc_method.settings['qc_method']:
            sections['**DALTON INPUT']['.RUN WAVE FUNCTION']
            sections['**INTEGRALS']['.DIPLEN']
            sections['**WAVE FUNCTIONS']['*CCEXCI']['.NCCEXC'] = properties.settings['excitation_energies']
            sections['**WAVE FUNCTIONS']['*CCOPA']['.DIPOLE']
            if 'cvseparation' in properties.settings:
                cvseparation = [[len(lst) for lst in properties.settings['cvseparation']]]
                cvseparation.extend(properties.settings['cvseparation'])
                cvs_lines = ''
                for line in cvseparation:
                    cvs_lines += f'{" ".join(map(str, line))}\n'
                sections['**WAVE FUNCTIONS']['*CCEXCI']['.CVSEPARATION'] = cvs_lines[:-1]
            if 'excitation_energies_triplet' in properties.settings and properties.settings['excitation_energies_triplet']:  # noqa: E501
                raise NotImplementedError('Triplet excitations are currently not supported for CC wave functions.')
        else:
            sections['**DALTON INPUT']['.RUN PROPERTIES']
            sections['**PROPERTIES']['.EXCITA']
            sections['**PROPERTIES']['*EXCITA']['.DIPSTR']
            sections['**PROPERTIES']['*EXCITA']['.NEXCIT'] = properties.settings['excitation_energies']
            if 'excitation_energies_triplet' in properties.settings and properties.settings['excitation_energies_triplet']:  # noqa: E501
                sections['**PROPERTIES']['*EXCITA']['.TRIPLET']
    if 'two_photon_absorption' in properties.settings:
        sections['**DALTON INPUT']['.RUN RESPONSE']
        sections['**RESPONSE']['*QUADRATIC']['.TWO-PHOTON']
        sections['**RESPONSE']['*QUADRATIC']['.ROOTS'] = properties.settings['two_photon_absorption']
        sections['**RESPONSE']['*QUADRATIC']['.ROOTS'] = properties.settings['two_photon_absorption']
    if 'hyperfine_couplings' in properties.settings:
        atoms = properties.settings['hyperfine_couplings']
        sections['**DALTON INPUT']['.RUN RESPONSE']
        sections['**INTEGRALS']['.FC']
        sections['**RESPONSE']['.TRPFLG']
        atoms_lines = ''
        for line in [[len(atoms)], atoms]:
            atoms_lines += f'{" ".join(map(str, line))}\n'
        sections['**RESPONSE']['*ESR']['.ATOMS'] = atoms_lines[:-1]
        sections['**RESPONSE']['*ESR']['.FCCALC']
    # Makes sure the orbital coefficients gets printed to the tar.gz
    sections['**WAVE FUNCTIONS']['*ORBITAL INPUT']['.PUNCHOUTORBITALS']
    sections['**END OF DALTON INPUT']
    # yapf: enable

    def parse_input_tree(section_tree: defaultdict, input_string: str = '') -> str:
        """Parse section tree and convert to Dalton input string

        Args:
            section_tree: A nested dict of dicts, which contains the activated
                dalton sections, subsections, keywords and values
            input_string: The string which becomes the final dalton input

        Returns:
            String of Dalton input.
        """
        # we need to make sure that ".OPTION" always comes before "*OPTION"
        # for example, someone could work first on *EXCITA, then decide to activate .ALPHA,
        # which could end up in the wrong place
        # sort keys such that "." comes before "*", otherwise leave them be.
        for key, value in sorted(section_tree.items(), key=lambda item: item[0][0], reverse=True):
            input_string += key + '\n'
            if isinstance(value, defaultdict):
                input_string = parse_input_tree(value, input_string)
            elif isinstance(value, list):
                input_string += f'{" ".join(map(str, value))}\n'
            else:
                input_string += f'{value}\n'
        return input_string

    dal_input = parse_input_tree(sections)
    return dal_input


def punch_orbitals(orbitals: Union[np.ndarray, Dict[int, np.ndarray]]) -> Tuple[str, List[int]]:
    """Write orbitals to the Dalton input file.

    Args:
        orbitals: Orbital coefficients to be included in the input file.

    Returns:
        Orbital coefficients formatted for the dal file, and string that tells
            if any orbitals are deleted.
    """
    if not isinstance(orbitals, dict):
        orbitals = {1: orbitals}
    orbitals_string = ''
    # delete_string has one element per symmetry.
    deleted_orbitals = [0] * len(orbitals)
    for symmetry, symmetry_orbitals in orbitals.items():
        if len(symmetry_orbitals) == 0:
            # Skip symmetries with zero orbitals.
            continue
        for i, symmetry_orbital in enumerate(np.transpose(symmetry_orbitals)):
            # Note transpose of coefficients because of the ordering of the orbitals in the dal file.
            if np.sum(np.abs(symmetry_orbital)) == 0:
                # An orbital is deleted if all the coefficients are zero.
                deleted_orbitals[symmetry - 1] += 1
            if i != 0:
                orbitals_string += '\n'
            for j, coefficient in enumerate(symmetry_orbital):
                if j != 0 and j % 4 == 0:
                    orbitals_string += '\n'
                if np.max(np.abs(symmetry_orbital)) < 100:
                    orbitals_string += f'{coefficient:18.14f}'
                elif np.max(np.abs(symmetry_orbital)) < 1000:
                    orbitals_string += f'{coefficient:18.13f}'
                elif np.max(np.abs(symmetry_orbital)) < 10000:
                    orbitals_string += f'{coefficient:18.12f}'
                elif np.max(np.abs(symmetry_orbital)) < 100000:
                    orbitals_string += f'{coefficient:18.11f}'
                elif np.max(np.abs(symmetry_orbital)) < 1000000:
                    orbitals_string += f'{coefficient:18.10f}'
                else:
                    raise ValueError('Orbital coefficient to large, orbitals from dal file will be broken.')
        orbitals_string += '\n'
    # Remove last newline.
    orbitals_string = orbitals_string[:-1]
    output = namedtuple('Orbitals', ['str_of_orbitals', 'num_deleted_orbitals'])
    return output(orbitals_string, deleted_orbitals)
