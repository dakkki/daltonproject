#  Copyright (C)  The Dalton Project Developers.
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#  Contact information can be found on our website: https://www.daltonproject.org/

from typing import Any, Dict, List, Optional, Union


class Property:
    """Define properties to be computed."""

    def __init__(self,
                 energy: bool = False,
                 dipole: bool = False,
                 polarizability: bool = False,
                 first_hyperpolarizability: bool = False,
                 gradients: Union[bool, dict] = False,
                 hessian: Union[bool, dict] = False,
                 geometry_optimization: Union[bool, dict] = False,
                 excitation_energies: Union[bool, dict] = False,
                 two_photon_absorption: Union[bool, dict] = False,
                 hyperfine_couplings: Optional[dict] = None,
                 dipole_gradients: bool = False,
                 polarizability_gradients: Union[bool, dict] = False) -> None:
        """Initialize Property instance.

        Args:
            energy: Calculate energy.
            gradients: Calculate molecular gradients.
            hessian: Activate molecular Hessian.
            geometry_optimization: Perform geometry optimization.
            excitation_energies: Calculate excitation energies.
            two_photon_absorption: Calculate two-photon absorption.
            hyperfine_couplings: Calculate hyperfine couplings.
            dipole_gradients: Calculate dipole gradients.
            polarizability_gradients: Calculate polarizability gradients.
        """
        self.settings: Dict[str, Any] = {}
        if energy:
            self.energy()
        if dipole:
            self.dipole()
        if polarizability:
            self.polarizability()
        if first_hyperpolarizability:
            self.first_hyperpolarizability()
        if isinstance(gradients, dict):
            self.gradients(**gradients)
        elif gradients is True:
            self.gradients()
        elif gradients is not False:
            raise TypeError(f'Wrong type: gradients={type(gradients).__name__}.')
        if isinstance(hessian, dict):
            self.hessian(**hessian)
        elif hessian is True:
            self.hessian()
        elif hessian is not False:
            raise TypeError(f'Wrong type: hessian={type(hessian).__name__}.')
        if isinstance(geometry_optimization, dict):
            self.geometry_optimization(**geometry_optimization)
        elif geometry_optimization is True:
            self.geometry_optimization()
        elif geometry_optimization is not False:
            raise TypeError(f'Wrong type: geometry_optimization={type(geometry_optimization).__name__}.')
        if isinstance(excitation_energies, dict):
            self.excitation_energies(**excitation_energies)
        elif excitation_energies is True:
            self.excitation_energies()
        elif excitation_energies is not False:
            raise TypeError(f'Wrong type: excitation_energies={type(excitation_energies).__name__}.')
        if isinstance(two_photon_absorption, dict):
            self.two_photon_absorption(**two_photon_absorption)
        elif two_photon_absorption is True:
            self.two_photon_absorption()
        elif two_photon_absorption is not False:
            raise TypeError(f'Wrong type: two_photon_absorption={type(two_photon_absorption).__name__}.')
        if isinstance(hyperfine_couplings, dict):
            self.hyperfine_couplings(**hyperfine_couplings)
        elif hyperfine_couplings is not None:
            raise TypeError(f'Wrong type: hyperfine_couplings={type(hyperfine_couplings).__name__}.')
        if dipole_gradients:
            self.dipole_gradients()
        if isinstance(polarizability_gradients, dict):
            self.polarizability_gradients(**polarizability_gradients)
        elif polarizability_gradients is True:
            self.polarizability_gradients()
        elif polarizability_gradients is not False:
            raise TypeError(f'Wrong type: polarizability_gradients={type(polarizability_gradients).__name__}.')

    def __repr__(self):
        # use list avoid doing string +=, since it scales quadratically in the number of elements
        repr_items = [f'{self.__class__.__name__}(']
        # used to work out padding
        last_idx = len(self.settings) - 1
        pad = ' ' * (len(self.__class__.__name__) + 1)
        for idx, (key, value) in enumerate(self.settings.items()):
            if idx == last_idx == 0:
                # if last_idx is 0 and idx is 0, then there is only a single
                # element (no padding, no comma, no newline)
                repr_items.append(f'{key}: {value}')
            elif idx == 0:
                # otherwise, we are printing the first normal element (newline and comma, but not padded)
                repr_items.append(f'{key}: {value},\n')
            elif idx == last_idx:
                # otherwise, if we arrive at last_idx (no newline and no comma)
                repr_items.append(f'{pad}{key}: {value}')
            else:
                # otherwise, we are printing a normal element (newline and comma)
                repr_items.append(f'{pad}{key}: {value},\n')
        repr_items.append(')')
        return ''.join(repr_items)

    def energy(self) -> None:
        """Energy."""
        self.settings.update({'energy': True})

    def dipole(self) -> None:
        """Dipole moment."""
        self.settings.update({'dipole': True})

    def polarizability(self) -> None:
        """Polarizability (alpha)."""
        self.settings.update({'polarizability': True})

    def first_hyperpolarizability(self) -> None:
        """First hyperpolarizability (beta)."""
        self.settings.update({'first_hyperpolarizability': True})

    def gradients(self, method: str = 'analytic') -> None:
        """Compute molecular gradients.

        Args:
            method: Method for computing gradients.
        """
        if not isinstance(method, str):
            raise TypeError('Gradient type must be given as a string')
        self.settings.update({'gradients': method.lower()})

    def hessian(self, method: str = 'analytic') -> None:
        """Compute molecular Hessian.

        Args:
            method: Method for computing Hessian.
        """
        if not isinstance(method, str):
            raise TypeError('Hessian type must be given as a string')
        self.settings.update({'hessian': method.lower()})

    def geometry_optimization(self, method: str = 'BFGS') -> None:
        """Geometry optimization.

        Args:
            method: Geometry optimization method.
        """
        if not isinstance(method, str):
            raise TypeError('Optimization method must be given as a string')
        self.settings.update({'geometry_optimization': method.upper()})

    def excitation_energies(self,
                            states: Union[int, List[int]] = 5,
                            triplet: bool = False,
                            cvseparation: Optional[Union[List[int], List[List[int]]]] = None) -> None:
        """Compute excitation energies.

        Args:
            states: Number of states.
            triplet: Turns on triplet excitations. Excitations are singlet as
                default.
            cvseparation: Core-valence separation. Specify the active orbitals
                of each symmetry species (irrep), giving the number of the
                orbitals. For example, [[1, 2], [1], [0]] specifies that
                orbitals 1 and 2 from the first irrep are active, orbital 1
                from the second irrep, and no orbitals from the third and last
                irrep.
        """
        if isinstance(states, int):
            states = [states]
        if not isinstance(states, list):
            raise TypeError('Number of states must be given as an integer '
                            'or list of integers if molecular symmetry is used.')
        if not isinstance(triplet, bool):
            raise TypeError('Triplet can be either True or False.')
        if cvseparation:
            if not isinstance(cvseparation, list):
                raise TypeError('Core-valence separation (cvseparation) must be given as a list of integers or a '
                                'list with lists of integers if molecular symmetry is used.')
            cvseparation_has_ints = [isinstance(element, int) for element in cvseparation]
            cvseparation_has_lists = [isinstance(element, list) for element in cvseparation]
            if not all(cvseparation_has_ints) and not all(cvseparation_has_lists):
                raise TypeError('Core-valence separation (cvseparation) must be given as a list of integers or a '
                                'list with lists of integers if molecular symmetry is used.')
            if all(cvseparation_has_ints):
                self.settings.update({'cvseparation': [cvseparation]})
            else:
                self.settings.update({'cvseparation': cvseparation})
        self.settings.update({'excitation_energies': states})
        if triplet or 'excitation_energies_triplet' in self.settings:
            if 'two_photon_absorption' in self.settings:
                raise TypeError('Triplet excitations cannot be combined with two-photon absorption.')
            self.settings.update({'excitation_energies_triplet': triplet})

    def two_photon_absorption(self, states: Union[int, List[int]] = 5) -> None:
        """Compute two-photon absorption strengths.

        Args:
            states: Number of states.
        """
        if isinstance(states, int):
            states = [states]
        if not isinstance(states, list):
            raise TypeError('Number of states must be given as an integer '
                            'or list of integers if molecular symmetry is used.')
        if 'excitation_energies_triplet' in self.settings:
            if self.settings['excitation_energies_triplet']:
                raise TypeError('Triplet excitations cannot be combined with two-photon absorption.')
        self.settings.update({'two_photon_absorption': states})

    def hyperfine_couplings(self, atoms: List[int]) -> None:
        """Compute hyperfine coupling constants.

        Args:
            atoms: List of atoms by index.
        """
        self.settings.update({'hyperfine_couplings': atoms})

    def dipole_gradients(self) -> None:
        """Compute gradients of the dipole with respect to nuclear displacements in Cartesian coordinates."""
        self.settings.update({'dipole_gradients': True})

    def polarizability_gradients(self, frequencies: Union[float, List[float]] = 0.0) -> None:
        """Compute gradients of the polarizability with respect to nuclear displacements in Cartesian coordinates.

        Args:
            frequencies: Frequencies at which the polarizability will be evaluated (hartree).
        """
        self.settings.update({'polarizability_gradients': True})
        if isinstance(frequencies, float):
            frequencies = [frequencies]
        if not isinstance(frequencies, list):
            raise TypeError('Frequencies must be given as a float or a list of floats.')
        self.settings.update({'polarizability_gradients_frequencies': frequencies})
