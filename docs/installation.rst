Installation
============

The Dalton Project package can be conveniently downloaded and installed using
**pip**::

    $ pip install [--user] daltonproject

The Dalton Project will in general expect the external program executables to
be in the **PATH** environment variable. The path to the executables can be
added to **PATH** using the following command::

    $ export PATH=/path/to/executable:${PATH}

An example of this could be for the Dalton program, where the program has been
compiled in the **build** directory::

    $ export PATH=/home/username/dalton/build:${PATH}

Note that the export command can be added to **~/.bashrc** to automatically be
run whenever a new terminal is opened.


Dalton
------

The `Dalton program <https://daltonprogram.org/>`__ can be downloaded from
`here <https://gitlab.com/dalton/dalton/>`__. See the README for download and
install instructions.


LSDalton
--------

The `LSDalton program <https://daltonprogram.org/>`__ can be downloaded from
`here <https://gitlab.com/dalton/lsdalton/>`__. See the README for download and
install instructions.


Developers
----------

Developers can clone the Dalton Project git repository which is hosted on
GitLab (`<https://gitlab/daltonproject/daltonproject>`__)::

    $ git clone https://gitlab.com/daltonproject/daltonproject.git

Alternatively, you can fork the project on GitLab and the clone the fork. After
the clone is downloaded you change directory to the root of the Dalton
Project::

    $ cd daltonproject

Then install the Dalton Project package in development mode which allows you to
try out your changes immediately using the installed package::

    $ pip install [--user] --editable .

Note that this will interfere with/overwrite any **pip** installed version of
the Dalton Project. The requirements needed for development can be installed
as::

    $ pip install [--user] -r requirements.txt

Before making any changes, make sure that all tests pass by running the test
suite (integration tests require that path to external libraries are in the
PATH environment variable)::

    $ pytest

The code linting and formatting tools can be setup to work automatically via
pre-commit hooks. To setup the pre-commit hooks run the following from the
root of the Dalton Project directory::

    $ pip install [--user] pre-commit
    $ pre-commit install

During a **git commit**, if any pre-commit hook fails, mostly you will simply
need to **git add** the affected files and **git commit** again, because most
tools will automatically reformat the files.
