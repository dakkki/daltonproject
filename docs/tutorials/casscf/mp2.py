import daltonproject as dp

molecule, basis = dp.mol_reader('pyridine.mol')
molecule.analyse_symmetry()

mp2 = dp.QCMethod('MP2')
prop = dp.Property(energy=True)
mp2_result = dp.dalton.compute(molecule, basis, mp2, prop)
print(mp2_result.filename)
# 52c2525a4e1901f5a77a81991e7cf92e2aeaf79e
print('MP2 energy =', mp2_result.energy)
# MP2 energy = -243.9889166118

if True:  # To keep test imports from going to the top of the file.
    import pathlib

    import numpy as np

assert pathlib.Path(mp2_result.filename).name == '52c2525a4e1901f5a77a81991e7cf92e2aeaf79e'
np.testing.assert_allclose(mp2_result.energy, -243.9889166118)
