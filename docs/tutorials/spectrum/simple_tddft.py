import matplotlib.pyplot as plt

import daltonproject as dp

molecule = dp.Molecule(input_file='water.xyz')
basis = dp.Basis(basis='cc-pVDZ')

dft = dp.QCMethod('DFT', 'CAMB3LYP')
prop = dp.Property(excitation_energies=True)
prop.excitation_energies(states=6)
result = dp.dalton.compute(molecule, basis, dft, prop)
print('Excitation energies =', result.excitation_energies)
# Excitation energies = [ 7.708981 10.457775 11.631572 11.928512 14.928328 15.733272]

print('Oscillator strengths =', result.oscillator_strengths)
# Oscillator strengths = [0.0255 0.     0.1324 0.0883 0.0787 0.171 ]

ax = dp.spectrum.plot_one_photon_spectrum(result, color='k')
plt.savefig('1pa.svg')

if True:
    import numpy as np
np.testing.assert_allclose(result.excitation_energies,
                           [7.708981, 10.457775, 11.631572, 11.928512, 14.928328, 15.733272])
np.testing.assert_allclose(result.oscillator_strengths, [0.0255, 0., 0.1324, 0.0883, 0.0787, 0.171])
