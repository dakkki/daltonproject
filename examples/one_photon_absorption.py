import numpy as np

import daltonproject as dp

hf = dp.QCMethod('HF')
basis = dp.Basis(basis='pcseg-0')
opa = dp.Property(excitation_energies=True)
water = dp.Molecule(input_file='data/water.xyz', charge=0)
result = dp.dalton.compute(water, basis, hf, opa)

frequencies = np.linspace(6.0, 20.0, 1000)
spectrum = dp.spectrum.convolute_one_photon_absorption(result.excitation_energies, result.oscillator_strengths,
                                                       frequencies)
print(spectrum)
