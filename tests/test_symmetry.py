from typing import List

import numpy as np
import pytest

import daltonproject as dp
from daltonproject.utilities import chdir


def check_if_molecule_is_same(xyz_original: np.ndarray,
                              xyz_new: np.ndarray,
                              labels_original: List[str],
                              labels_new: List[str],
                              threshold: float = 10**-3) -> bool:
    """Checks if two molecules are identical within a threshold.

    Args:
      xyz_original: Molecule coordinates.
      xyz_new: Molecule coordinates.
      labels_original: Labels.
      labels_new: Labels.
      threshold: Threshold for molecules being identical.

    Returns:
      True, if the molecules are identical.
    """
    if len(xyz_original) != len(xyz_new):
        return False
    elif len(xyz_original) != len(labels_original):
        return False
    elif len(xyz_original) != len(labels_new):
        return False
    same_check = True
    atom_same_check = np.zeros(len(xyz_original))
    for i in range(len(xyz_original)):
        min_distance2 = 10**12
        for j in range(len(xyz_new)):
            if labels_original[i] != labels_new[j]:
                continue
            if atom_same_check[j] != 0:
                continue
            distance2 = np.sum((xyz_original[i] - xyz_new[j])**2)
            if distance2 < min_distance2:
                idx_closest = j
                min_distance2 = distance2
                if min_distance2**0.5 < threshold:
                    break
        if min_distance2**0.5 < threshold:
            atom_same_check[idx_closest] = 1
        else:
            same_check = False
            break
    return same_check


@pytest.mark.datafiles(pytest.DATADIR / 'Hypochloric_Acid.xyz')
def test_multiple_writes(datafiles):
    """Test that the written files are identical if two files are written.

    Test made after a bug where the elements of the molecule would change when writing the mol-file.
    """
    with chdir(datafiles):
        mol = dp.Molecule(input_file=(datafiles / 'Hypochloric_Acid.xyz'), symmetry=True)
        dp.dalton.program.write_molecule_input(mol, dp.Basis('3-21G'), 'tmp')
        molecule, basis = dp.mol_reader('tmp.mol')
        dp.dalton.program.write_molecule_input(mol, dp.Basis('3-21G'), 'tmp2')
        molecule2, basis2 = dp.mol_reader('tmp2.mol')
    assert check_if_molecule_is_same(molecule.coordinates, molecule2.coordinates, molecule.labels,
                                     molecule2.labels)


@pytest.mark.datafiles(
    pytest.DATADIR / 'water_orientation1.mol',
    pytest.DATADIR / 'water_orientation1.xyz',
    pytest.DATADIR / 'water_orientation2.xyz',
    pytest.DATADIR / 'water_orientation3.xyz',
)
def test_c2v_different_orientations(datafiles):
    """Test that different initial orientations of water leads to the same input."""
    with chdir(datafiles):
        mol = dp.Molecule(input_file=(datafiles / 'water_orientation1.xyz'), symmetry=True)
        dp.dalton.program.write_molecule_input(mol, dp.Basis('3-21G'), 'tmp')
        molecule, basis = dp.mol_reader('tmp.mol')
        ref_molecule, ref_basis = dp.mol_reader(datafiles / 'water_orientation1.mol')
    assert check_if_molecule_is_same(molecule.coordinates, ref_molecule.coordinates, molecule.labels,
                                     ref_molecule.labels)
    with chdir(datafiles):
        mol = dp.Molecule(input_file=(datafiles / 'water_orientation2.xyz'), symmetry=True)
        dp.dalton.program.write_molecule_input(mol, dp.Basis('3-21G'), 'tmp')
        molecule, basis = dp.mol_reader('tmp.mol')
        ref_molecule, ref_basis = dp.mol_reader(datafiles / 'water_orientation1.mol')
    assert check_if_molecule_is_same(molecule.coordinates, ref_molecule.coordinates, molecule.labels,
                                     ref_molecule.labels)
    with chdir(datafiles):
        mol = dp.Molecule(input_file=(datafiles / 'water_orientation3.xyz'), symmetry=True)
        dp.dalton.program.write_molecule_input(mol, dp.Basis('3-21G'), 'tmp')
        molecule, basis = dp.mol_reader('tmp.mol')
        ref_molecule, ref_basis = dp.mol_reader(datafiles / 'water_orientation1.mol')
    assert check_if_molecule_is_same(molecule.coordinates, ref_molecule.coordinates, molecule.labels,
                                     ref_molecule.labels)


def label_test(molname, group, dump_mol):
    return f'{group}_{molname}{"_read_back_mol" if dump_mol else ""}'


args = [
    ('not_methanol', 'C1', False),
    ('He', 'K', True),
    ('Hypochloric_Acid', 'Cs', False),
    ('S7-Sulfur', 'Cs', False),
    ('Quinoline', 'Cs', True),
    ('meso-tartaric_acid', 'Ci', False),
    ('staggered-1,2-Dibromo-1,2-dichloroethane', 'Ci', True),
    ('hydrogenperoxide', 'C2', False),
    ('1,3-dichloroallene', 'C2', False),
    ('half-chair-cyclopentane', 'C2', True),
    ('rotated-1,1,1-Trichloroethane', 'C3', False),
    ('Triphenylmethane', 'C3', False),
    ('Biphenyl', 'D2', False),
    ('Twistane', 'D2', True),
    ('rotated-ethane', 'D3', True),
    ('rotated-ferrocene', 'D5', True),
    ('rotated-Bis(benzene)chromium', 'D6', True),
    ('rotated-uranocene', 'D8', True),
    ('water', 'C2v', False),
    ('Squaric_Acid_Difluoride', 'C2v', False),
    ('Phenanthrene', 'C2v', True),
    ('Ammonia', 'C3v', False),
    ('1-Azabicyclo[2.2.2]octane', 'C3v', False),
    ('Sumanene', 'C3v', True),
    ('Brominepentafluoride', 'C4v', False),
    ('Sulfurchloridepentafluoride', 'C4v', False),
    ('9-Pentaborane', 'C4v', True),
    ('Corannulene', 'C5v', True),
    ('trans-1,2-Dichloroethylene', 'C2h', False),
    ('s-trans-Butadiene', 'C2h', False),
    ('Chrysene', 'C2h', True),
    ('Boric_Acid', 'C3h', False),
    ('Tricyclo[3.3.3.0(1,5)]undecane', 'C3h', False),
    ('Bicyclo[3.3.3]undecane', 'C3h', True),
    ('Ethylene', 'D2h', False),
    ('Tricyclo[3.1.1.1(2,4)]octane', 'D2h', False),
    ('Dianthracene', 'D2h', True),
    ('Borontrifluoride', 'D3h', False),
    ('Ironpentacarbonyl', 'D3h', False),
    ('Triptycene', 'D3h', True),
    ('[Al4]2--Ion', 'D4h', False),
    ('Xenontetrafluoride', 'D4h', False),
    ('Squaric_Acid_Dianion', 'D4h', True),
    ('Iodineheptafluoride', 'D5h', False),
    ('eclipsed-Ferrocene', 'D5h', False),
    ('C70-Fullerene', 'D5h', True),
    ('Benzene', 'D6h', False),
    ('Superphane', 'D6h', False),
    ('Cucurbit[6]uril', 'D6h', True),
    ('Tropylium_Cation', 'D7h', True),
    ('Cyclooctatetraene_Dianion', 'D8h', False),
    ('Sulflower', 'D8h', False),
    ('eclipsed-Uranocene', 'D8h', True),
    ('Allene', 'D2d', False),
    ('Cyclobutane', 'D2d', False),
    ('Cyclooctatetraene', 'D2d', True),
    ('S6-Sulfur', 'D3d', False),
    ('chair-Cyclohexane', 'D3d', False),
    ('Hexamethylbenzene', 'D3d', True),
    ('S8-Sulfur', 'D4d', True),
    ('staggered-Uranocene', 'D8d', True),
    ('1,3,5,7-Tetrachlorocyclooctatetraene', 'S4', False),
    ('Tetrachloroneopentane', 'S4', False),
    ('Tetraphenylmethane', 'S4', True),
    ('rotated-neopentane', 'T', False),
    ('C60F36-Fullerene', 'T', True),
    ('[Mg(H2O)6]2+-Ion', 'Th', False),
    ('C60Br24-Bromofullerene', 'Th', True),
    ('Phosphorus', 'Td', False),
    ('Phosphoruspentoxide', 'Td', False),
    ('Tetrairidiumdodecacarbonyl', 'Td', True),
    ('rotated-Octamethylsilsesquioxane', 'O', False),
    ('Dodecaethyleneoctamine', 'O', True),
    ('Sulfurhexafluoride', 'Oh', False),
    ('Tungstenhexacarbonyl', 'Oh', False),
    ('Octamethylsilsesquioxane', 'Oh', True),
    ('C60-Fullerene', 'Ih', True),
    ('Hydrogen_Chloride', 'Coov', False),
    ('Fluoroacetylene', 'Coov', False),
    ('Fluorodiacetylene', 'Coov', True),
    ('Hydrogen', 'Dooh', False),
    ('Acetylene', 'Dooh', False),
    ('Diacetylene', 'Dooh', True),
]


@pytest.mark.parametrize(
    'molname,group,dump_mol',
    [
        pytest.param(molname, group, dump_mol, id=label_test(molname, group, dump_mol))
        for molname, group, dump_mol in args
    ],
)
def test_point_group(molname, group, dump_mol, tmpdir):
    with chdir(tmpdir):
        mol = dp.Molecule(input_file=(pytest.DATADIR / f'{molname}.xyz'), symmetry=True)
        assert mol.point_group == group
        if dump_mol:
            dp.dalton.program.write_molecule_input(mol, dp.Basis('3-21G'), 'tmp')
            molecule, basis = dp.mol_reader('tmp.mol')
            ref_molecule, ref_basis = dp.mol_reader(pytest.DATADIR / f'{molname}.mol')
            assert check_if_molecule_is_same(molecule.coordinates, ref_molecule.coordinates, molecule.labels,
                                             ref_molecule.labels)


@pytest.mark.datafiles(
    pytest.DATADIR / 'staggered-Ferrocene.xyz',
    pytest.DATADIR / 'staggered-Ferrocene.mol',
)
def test_d5d_point_group(datafiles):
    with chdir(datafiles):
        mol = dp.Molecule(input_file=(datafiles / 'staggered-Ferrocene.xyz'), symmetry=True)
        dp.dalton.program.write_molecule_input(mol, dp.Basis('3-21G'), 'tmp')
        molecule, basis = dp.mol_reader('tmp.mol')
        ref_molecule, ref_basis = dp.mol_reader(datafiles / 'staggered-Ferrocene.mol')
        ref2_coordinates = ref_molecule.coordinates.copy()
        ref2_coordinates[:, 2] = -ref2_coordinates[:, 2]
    assert mol.point_group == 'D5d'
    assert check_if_molecule_is_same(molecule.coordinates, ref_molecule.coordinates, molecule.labels,
                                     ref_molecule.labels) or check_if_molecule_is_same(
                                         molecule.coordinates, ref2_coordinates, molecule.labels, ref_molecule.labels)


@pytest.mark.datafiles(
    pytest.DATADIR / 'staggered-Bis(benzene)chromium.xyz',
    pytest.DATADIR / 'staggered-Bis(benzene)chromium.mol',
)
def test_d6d_point_group(datafiles):
    with chdir(datafiles):
        mol = dp.Molecule(input_file=(datafiles / 'staggered-Bis(benzene)chromium.xyz'), symmetry=True)
        dp.dalton.program.write_molecule_input(mol, dp.Basis('3-21G'), 'tmp')
        molecule, basis = dp.mol_reader('tmp.mol')
        ref_molecule, ref_basis = dp.mol_reader(datafiles / 'staggered-Bis(benzene)chromium.mol')
        ref2_coordinates = ref_molecule.coordinates.copy()
        ref2_coordinates[:, 2] = -ref2_coordinates[:, 2]
    assert mol.point_group == 'D6d'
    assert check_if_molecule_is_same(molecule.coordinates, ref_molecule.coordinates, molecule.labels,
                                     ref_molecule.labels) or check_if_molecule_is_same(
                                         molecule.coordinates, ref2_coordinates, molecule.labels, ref_molecule.labels)
