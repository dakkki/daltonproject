from pathlib import Path

import pytest


def pytest_configure(config):
    # NOTE This is a workaround for https://github.com/omarkohl/pytest-datafiles/pull/9
    config.addinivalue_line('markers', 'datafiles(path): path to tmp data files')
    pytest.DATADIR = (Path(__file__).parent / 'data').resolve()
