import textwrap

import numpy as np
import pytest

import daltonproject as dp
from daltonproject.utilities import chdir


@pytest.mark.datafiles(
    pytest.DATADIR / 'water.xyz', )
def test_dalton_tpa(datafiles):
    hf = dp.QCMethod('HF')
    basis = dp.Basis(basis='pcseg-0')
    energy = dp.Property(energy=True, two_photon_absorption=True)
    water = dp.Molecule(input_file=(datafiles / 'water.xyz'))
    with chdir(datafiles):
        settings = dp.ComputeSettings(mpi_num_procs=1, omp_num_threads=1)
        result_1 = dp.dalton.compute(molecule=water,
                                     basis=basis,
                                     qc_method=hf,
                                     properties=energy,
                                     compute_settings=settings)
    np.testing.assert_allclose(result_1.energy, -75.702096519278)
    np.testing.assert_allclose(result_1.excitation_energies,
                               [9.21080934, 12.0584537, 12.58553825, 13.41155621, 16.55956801])
    np.testing.assert_allclose(result_1.two_photon_strengths, [1.18, 2.48, 6.39, 14.2, 14.9])
    np.testing.assert_allclose(result_1.two_photon_cross_sections, [0.0732, 0.264, 0.741, 1.87, 2.99])
    elements_s1 = np.array([[-0.0, 2.1, 0.0], [2.1, -0.0, -0.0], [0.0, -0.0, 0.0]])
    elements_s2 = np.array([[0.0, 0.0, 0.0], [0.0, 0.0, -3.1], [0.0, -3.1, -0.0]])
    elements_s3 = np.array([[0.0, 0.0, 4.9], [0.0, 0.0, -0.0], [4.9, -0.0, 0.0]])
    elements_s4 = np.array([[-6.9, 0.0, 0.0], [0.0, -1.1, -0.0], [0.0, -0.0, -2.3]])
    elements_s5 = np.array([[0.0, 0.0, -7.5], [0.0, 0.0, 0.0], [-7.5, 0.0, 0.0]])
    assert np.allclose(result_1.two_photon_tensors[0], elements_s1) \
        or np.allclose(result_1.two_photon_tensors[0], -elements_s1)
    assert np.allclose(result_1.two_photon_tensors[1], elements_s2) \
        or np.allclose(result_1.two_photon_tensors[1], -elements_s2)
    assert np.allclose(result_1.two_photon_tensors[2], elements_s3) \
        or np.allclose(result_1.two_photon_tensors[2], -elements_s3)
    assert np.allclose(result_1.two_photon_tensors[3], elements_s4) \
        or np.allclose(result_1.two_photon_tensors[3], -elements_s4)
    assert np.allclose(result_1.two_photon_tensors[4], elements_s5) \
        or np.allclose(result_1.two_photon_tensors[4], -elements_s5)
    with pytest.raises(ValueError, match='One-photon oscillator strengths not available from TPA calculation.'):
        result_1.oscillator_strengths
    excita = dp.Property(excitation_energies=True)
    with chdir(datafiles):
        result_2 = dp.dalton.compute(molecule=water,
                                     basis=basis,
                                     qc_method=hf,
                                     properties=excita,
                                     compute_settings=settings)
    np.testing.assert_allclose(result_1.excitation_energies, result_2.excitation_energies, atol=1e-5)


@pytest.mark.datafiles(
    pytest.DATADIR / 'water.xyz', )
def test_dalton_dipole(datafiles):
    hf = dp.QCMethod('HF')
    basis = dp.Basis(basis='pcseg-0')
    prop = dp.Property(dipole=True)
    water = dp.Molecule(input_file=(datafiles / 'water.xyz'))
    with chdir(datafiles):
        settings = dp.ComputeSettings(mpi_num_procs=1, omp_num_threads=1)
        result = dp.dalton.compute(molecule=water,
                                   basis=basis,
                                   qc_method=hf,
                                   properties=prop,
                                   compute_settings=settings)
    np.testing.assert_allclose(result.dipole, [1.17001531, 0., 0.])


@pytest.mark.datafiles(
    pytest.DATADIR / 'water.xyz', )
def test_dalton_polarizability(datafiles):
    hf = dp.QCMethod('HF')
    basis = dp.Basis(basis='pcseg-0')
    prop = dp.Property(polarizability=True)
    water = dp.Molecule(input_file=(datafiles / 'water.xyz'))
    with chdir(datafiles):
        settings = dp.ComputeSettings(mpi_num_procs=1, omp_num_threads=1)
        result = dp.dalton.compute(molecule=water,
                                   basis=basis,
                                   qc_method=hf,
                                   properties=prop,
                                   compute_settings=settings)
    ref_alpha = np.array([[5.48684006e+00, 0., 0.], [0., 1.47177553e+00, 0.], [0., 0., 4.40273540e+00]])
    np.testing.assert_allclose(result.polarizability, ref_alpha, atol=1e-8)


@pytest.mark.datafiles(
    pytest.DATADIR / 'water.xyz', )
def test_dalton_first_hyperpolarizability(datafiles):
    hf = dp.QCMethod('HF')
    basis = dp.Basis(basis='pcseg-0')
    prop = dp.Property(first_hyperpolarizability=True)
    water = dp.Molecule(input_file=(datafiles / 'water.xyz'))
    with chdir(datafiles):
        settings = dp.ComputeSettings(mpi_num_procs=1, omp_num_threads=1)
        result = dp.dalton.compute(molecule=water,
                                   basis=basis,
                                   qc_method=hf,
                                   properties=prop,
                                   compute_settings=settings)
    ref_beta = np.array([[[0., 0., 0.], [-0., -1.23851149, -0.], [0., -0., -18.90903982]],
                         [[0., -1.2385114, -0.], [-1.2385114, -0., 0.], [-0., 0., -0.]],
                         [[0., -0., -18.90903981], [-0., 0., -0.], [-18.90903981, -0., 0.]]])
    np.testing.assert_allclose(result.first_hyperpolarizability, ref_beta)


def test_dalton_hfc(tmpdir):
    dft = dp.QCMethod('DFT', 'BP86')
    mol = dp.Molecule(atoms=textwrap.dedent("""Ti    0.000000  0.000000   0.000000
                                               F     1.756834  0.000000   0.000000
                                               F    -0.878417  1.521463   0.000000
                                               F    -0.878417 -1.521463   0.000000"""))
    basis = dp.Basis(basis='STO-3G')
    prop = dp.Property(energy=True, hyperfine_couplings={'atoms': [1, 2]})
    with chdir(tmpdir):
        settings = dp.ComputeSettings(mpi_num_procs=1, omp_num_threads=1)
        result = dp.dalton.compute(molecule=mol,
                                   basis=basis,
                                   qc_method=dft,
                                   properties=prop,
                                   compute_settings=settings)
    np.testing.assert_allclose(result.energy, -1136.250771376731)
    np.testing.assert_allclose(result.hyperfine_couplings.polarization, [0.395462, -0.006226], atol=1e-5)
    np.testing.assert_allclose(result.hyperfine_couplings.direct, [14.941623, 0.318802], atol=1e-5)
    np.testing.assert_allclose(result.hyperfine_couplings.total, [15.337085, 0.312577], atol=1e-5)


@pytest.mark.datafiles(
    pytest.DATADIR / 'water.xyz',
    pytest.DATADIR / 'Ammonia.xyz',
    pytest.DATADIR / 'Acetylene.xyz',
    pytest.DATADIR / 'ethanol.xyz',
)
def test_dalton_compute_farm(datafiles):
    hf = dp.QCMethod(qc_method='HF')
    pcseg0 = dp.Basis(basis='pcseg-0')
    energy = dp.Property(energy=True)
    molecules = []
    for molecule in ['water', 'Ammonia', 'Acetylene', 'ethanol']:
        molecules.append(dp.Molecule(input_file=(datafiles / f'{molecule}.xyz')))
    with chdir(datafiles):
        settings = dp.ComputeSettings(jobs_per_node=2)
        results = dp.dalton.compute_farm(molecules=molecules,
                                         basis=pcseg0,
                                         qc_method=hf,
                                         properties=energy,
                                         compute_settings=settings)
    ref_energies = [-75.702096519278, -56.014788641629, -76.616128280751, -153.616385456613]
    for result, ref_energy in zip(results, ref_energies):
        assert result.energy == pytest.approx(ref_energy)


@pytest.mark.datafiles(
    pytest.DATADIR / 'water.xyz', )
def test_lsdalton_program(datafiles):
    b3lyp = dp.QCMethod(qc_method='DFT', xc_functional='B3LYP', coulomb='DF', exchange='ADMM')
    basis = dp.Basis(basis='pcseg-0', ri='def2-universal-JKFIT', admm='admm-1')
    geo = dp.Property(geometry_optimization=True)
    water = dp.Molecule(input_file=(datafiles / 'water.xyz'))
    with chdir(datafiles):
        # Geometry optimization
        result = dp.lsdalton.compute(molecule=water, basis=basis, qc_method=b3lyp, properties=geo)
    ref_geometry = np.array([[0.1006425273649375, -0.0, -4.62e-14], [0.7082807363175291, -0.0, 0.7948045550166909],
                             [0.7082807363175332, -0.0, -0.7948045550166447]])
    np.testing.assert_allclose(result.energy, -76.143018932617, atol=1e-8)
    np.testing.assert_allclose(result.final_geometry, ref_geometry, atol=1e-5)
    water.coordinates = result.final_geometry
    exci = dp.Property(excitation_energies=True)
    with chdir(datafiles):
        # Excitation energies
        result = dp.lsdalton.compute(molecule=water, basis=basis, qc_method=b3lyp, properties=exci)
    ref_excitation_energies = [6.9031199755, 9.1249065328, 9.1885716527, 11.5216193097, 13.6848461475]
    ref_oscillator_strengths = [0.00561954, 0.0, 0.08407087, 0.08261734, 0.38885365]
    np.testing.assert_allclose(result.excitation_energies, ref_excitation_energies, atol=2e-4)
    np.testing.assert_allclose(result.oscillator_strengths, ref_oscillator_strengths, atol=2e-4)


@pytest.mark.datafiles(
    pytest.DATADIR / 'water.xyz',
    pytest.DATADIR / 'Ammonia.xyz',
    pytest.DATADIR / 'Acetylene.xyz',
    pytest.DATADIR / 'ethanol.xyz',
)
def test_lsdalton_compute_farm(datafiles):
    hf = dp.QCMethod(qc_method='HF')
    pcseg0 = dp.Basis(basis='pcseg-0')
    energy = dp.Property(energy=True)
    molecules = []
    for molecule in ['water', 'Ammonia', 'Acetylene', 'ethanol']:
        molecules.append(dp.Molecule(input_file=(datafiles / f'{molecule}.xyz')))
    with chdir(datafiles):
        settings = dp.ComputeSettings(jobs_per_node=2)
        results = dp.lsdalton.compute_farm(molecules=molecules,
                                           basis=pcseg0,
                                           qc_method=hf,
                                           properties=energy,
                                           compute_settings=settings)
    ref_energies = [-75.702096521109, -56.014788640904, -76.616128280277, -153.616385455019]
    for result, ref_energy in zip(results, ref_energies):
        assert result.energy == pytest.approx(ref_energy)


@pytest.mark.datafiles(
    pytest.DATADIR / 'water_opt.xyz', )
def test_lsdalton_openrsp(datafiles):
    """LSDalton/OpenRSP Hessian and dipole & polarizability gradients."""
    # yapf: disable
    ref_hessian = np.array(
        [[0.4179979608391009, 4.597240269481922e-11, -1.787207132763513e-16,
          -0.2089989801603071, -1.3078243384213713e-11, -0.18184786680942328,
          -0.20899898016031004, -3.4097351514262484e-12, 0.18184786680942994],
         [4.597240269481922e-11, -1.916231151033454e-06, -3.916551645655743e-17,
          -2.480066075144176e-13, 9.567768217283446e-07, -3.1429400332505866e-11,
          -2.4803023749050665e-13, 9.567768166213186e-07, 3.1429439485008946e-11],
         [-1.787207132763513e-16, -3.916551645655743e-17, 0.7009202724206388,
          -0.24978025704130696, 4.351495621329489e-12, -0.35046013602377635,
          0.24978025704131374, -2.3025666415707368e-12, -0.3504601360237835],
         [-0.2089989801603071, -2.480066075144176e-13, -0.24978025704130696,
          0.1935105203845061, 9.708906587183846e-12, 0.21581406171546658,
          0.015488459775816501, -2.240727273814105e-12, 0.03396619490607261],
         [-1.3078243384213713e-11, 9.567768217283446e-07, 4.351495621329489e-12,
          9.708906587183846e-12, -1.0080268589562902e-06, 1.5372179716545295e-11,
          -2.1030530718976464e-12, 5.12500835936347e-08, -1.6057232978767714e-11],
         [-0.18184786680942328, -3.1429400332505866e-11, -0.35046013602377635,
          0.21581406171546658, 1.5372179716545295e-11, 0.38517809626514843,
          -0.03396619499656099, 2.2456793702100777e-11, -0.03471796024134734],
         [-0.20899898016031004, -2.4803023749050665e-13, 0.24978025704131374,
          0.015488459775816501, -2.1030530718976464e-12, -0.03396619499656099,
          0.19351052038449912, 5.650462425232104e-12, -0.21581406171548217],
         [-3.4097351514262484e-12, 9.567768166213186e-07, -2.3025666415707368e-12,
          -2.240727273814105e-12, 5.12500835936347e-08, 2.2456793702100777e-11,
          5.650462425232104e-12, -1.0080268848938756e-06, -1.5372206506243435e-11],
         [0.18184786680942994, 3.1429439485008946e-11, -0.3504601360237835,
          0.03396619490607261, -1.6057232978767714e-11, -0.03471796024134734,
          -0.21581406171548217, -1.5372206506243435e-11, 0.38517809626515165]]
    )
    ref_dipole_gradients = np.array(
        [[0.4171292002495422, 2.003412481902576e-11, -3.596821735699057e-14],
         [2.6866043499501784e-10, 0.9154224965231719, 3.779864280820223e-18],
         [4.3231545188322784e-15, 1.4866940618719938e-11, 0.5518838782963273],
         [-0.20856460012477016, -1.757435450563522e-11, 0.12955042701613118],
         [-1.3433022970027398e-10, -0.45771124826159415, -1.1625058984855846e-10],
         [0.07733194628562962, 2.391801191652093e-11, -0.2759419391481778],
         [-0.20856460012478534, -2.4597703133821896e-12, -0.12955042701614128],
         [-1.34330205294736e-10, -0.4577112482615918, 1.1625058606871043e-10],
         [-0.07733194628562891, -3.8784952535241054e-11, -0.2759419391481804]]
    )
    ref_polarizability_gradients = np.array(
        [[[6.130330194817337, 1.4398652977853871e-10, 1.085337900201567e-13],
          [1.4398652977853871e-10, -0.5067981296877658, 2.3522135143666447e-16],
          [1.085337900201567e-13, 2.3522135143666447e-16, 6.154145466646408]],
         [[-7.317150130690141e-10, 2.32970311216172, -3.1394096125579907e-16],
          [2.32970311216172, -5.612531941347934e-10, -3.6664798801399067e-13],
          [-3.1394096125579907e-16, -3.6664798801399067e-13, -2.027270917846008e-09]],
         [[-2.1759990117826804e-14, 2.232825849930165e-16, 4.806098554546317],
          [2.232825849930165e-16, 1.454290216061041e-15, 8.15290627610043e-10],
          [4.806098554546317, 8.15290627610043e-10, -1.1653988939578698e-12]],
         [[-3.0651650974088724, -7.199326950434005e-11, -2.7215265334734466],
          [-7.199326950434005e-11, 0.2533990648438953, -3.603988898412281e-10],
          [-2.7215265334734466, -3.603988898412281e-10, -3.077072733323348]],
         [[3.658575407477692e-10, -1.1648515560808668, 1.0915842499079552e-10],
          [-1.1648515560808668, 2.8062674462670887e-10, -1.8390395773712909],
          [1.0915842499079552e-10, -1.8390395773712909, 1.0136357646279874e-09]],
         [[-0.7882476810764802, -2.568931960958996e-10, -2.4030492772733547],
          [-2.568931960958996e-10, 0.3496304289670143, -4.076454668662795e-10],
          [-2.4030492772733547, -4.076454668662795e-10, -5.855866924711051]],
         [[-3.0651650974086975, -7.199326027425372e-11, 2.721526533473344],
          [-7.199326027425372e-11, 0.253399064843899, 3.6039865461984156e-10],
          [2.721526533473344, 3.6039865461984156e-10, -3.077072733323435]],
         [[3.658574723212201e-10, -1.164851556080872, -1.091581110498379e-10],
          [-1.164851556080872, 2.806264495080762e-10, 1.839039577371656],
          [-1.091581110498379e-10, 1.839039577371656, 1.0136351532182234e-09]],
         [[0.7882476810765124, 2.5689297281329516e-10, -2.403049277272231],
          [2.5689297281329516e-10, -0.34963042896701, -4.0764516074382043e-10],
          [-2.403049277272231, -4.0764516074382043e-10, 5.855866924712902]]]
    )
    # yapf: enable
    hf = dp.QCMethod('HF')
    hf.scf_threshold(threshold=1e-8)
    basis = dp.Basis(basis='pcseg-0')
    prop = dp.Property(hessian=True, dipole_gradients=True, polarizability_gradients=True)
    prop.polarizability_gradients(frequencies=[0.01])
    water = dp.Molecule(input_file=(datafiles / 'water_opt.xyz'))
    with chdir(datafiles):
        result = dp.lsdalton.compute(molecule=water, basis=basis, qc_method=hf, properties=prop)
    np.testing.assert_allclose(result.hessian, ref_hessian, atol=1e-8)
    np.testing.assert_allclose(result.dipole_gradients, ref_dipole_gradients, atol=1e-8)
    np.testing.assert_allclose(result.polarizability_gradients.values[0], ref_polarizability_gradients, atol=1e-8)
    assert result.polarizability_gradients.frequencies[0] == 0.01
