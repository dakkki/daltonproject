import shutil
import textwrap

import numpy as np
import pytest

import daltonproject as dp
from daltonproject.utilities import chdir


@pytest.mark.datafiles(
    pytest.DATADIR / 'water.xyz', )
def test_compute_exceptions(datafiles):
    hf = dp.QCMethod('HF')
    basis = dp.Basis(basis='3-21G')
    energy = dp.Property(energy=True)
    water = dp.Molecule(input_file=(datafiles / 'water.xyz'))
    with pytest.raises(TypeError):
        dp.dalton.compute(water, basis, basis, energy)
    with pytest.raises(TypeError):
        dp.dalton.compute(water, hf, hf, energy)
    with pytest.raises(TypeError):
        dp.dalton.compute(hf, basis, hf, energy)
    with pytest.raises(TypeError):
        dp.dalton.compute(water, basis, hf, hf)
    if not shutil.which(cmd='dalton'):
        with pytest.raises(FileNotFoundError, match='dalton script was not found or is not executable.'):
            dp.dalton.compute(water, basis, hf, energy)


@pytest.mark.datafiles(
    pytest.DATADIR / 'input_He_nosym.out',
    pytest.DATADIR / 'input_He_nosym.tar.gz',
    pytest.DATADIR / 'input_He.out',
    pytest.DATADIR / 'input_He.tar.gz',
)
def test_orbital_coefficients(datafiles):
    output = dp.dalton.OutputParser(datafiles / 'input_He_nosym')
    reference = np.array([[1.00000010e00, -1.93982765e00, 0.00000000e00, 0.00000000e00, 0.00000000e00],
                          [-1.15851440e-07, 2.18241415e00, 0.00000000e00, 0.00000000e00, 0.00000000e00],
                          [0.00000000e00, 0.00000000e00, 0.00000000e00, 0.00000000e00, 1.00000000e00],
                          [0.00000000e00, 0.00000000e00, 1.00000000e00, 0.00000000e00, 0.00000000e00],
                          [0.00000000e00, 0.00000000e00, 0.00000000e00, 1.00000000e00, 0.00000000e00]])
    coeff = output.orbital_coefficients
    np.testing.assert_allclose(reference, coeff, atol=1e-7)
    output = dp.dalton.OutputParser(datafiles / 'input_He')
    reference = {
        1: np.array([[1.00000010e00, -1.93982765e00], [-1.16153510e-07, 2.18241415e00]]), 2: np.array([[1.0]]),
        3: np.array([[1.0]]), 5: np.array([[1.0]])
    }
    coeff = output.orbital_coefficients
    np.testing.assert_allclose(reference[1], coeff[1], atol=1e-7)
    np.testing.assert_allclose(reference[2], coeff[2], atol=1e-7)
    np.testing.assert_allclose(reference[3], coeff[3], atol=1e-7)
    np.testing.assert_allclose(reference[5], coeff[5], atol=1e-7)


@pytest.mark.datafiles(
    pytest.DATADIR / 'empty.tar.gz', )
def test_orbital_coefficients_exceptions(datafiles):
    output = dp.dalton.OutputParser(datafiles / 'empty')
    with pytest.raises(Exception, match='Could not find orbital file'):
        output.orbital_coefficients


@pytest.mark.datafiles(
    pytest.DATADIR / 'input_He_nosym.out',
    pytest.DATADIR / 'input_He_nosym.tar.gz',
    pytest.DATADIR / 'input_He.out',
    pytest.DATADIR / 'input_He.tar.gz',
)
def test_num_orbitals(datafiles):
    output = dp.dalton.OutputParser(datafiles / 'input_He_nosym')
    orbitals = output.num_orbitals
    orbitals_reference = (5, [5])
    assert orbitals == orbitals_reference
    output = dp.dalton.OutputParser(datafiles / 'input_He')
    orbitals = output.num_orbitals
    orbitals_reference = (5, [2, 1, 1, 0, 1, 0, 0, 0])
    assert orbitals == orbitals_reference


@pytest.mark.datafiles(
    pytest.DATADIR / 'input_He_nosym.out',
    pytest.DATADIR / 'input_He.out',
)
def test_num_basis_functions(datafiles):
    output = dp.dalton.OutputParser(datafiles / 'input_He_nosym')
    bf = output.num_basis_functions
    bf_reference = (5, [5])
    assert bf == bf_reference
    output = dp.dalton.OutputParser(datafiles / 'input_He')
    bf = output.num_basis_functions
    bf_reference = (5, [2, 1, 1, 0, 1, 0, 0, 0])
    assert bf == bf_reference


def test_dft_exceptions(datafiles):
    qcmethod = dp.QCMethod('DFT', 'PBE')
    qcmethod.exact_exchange(0.2)
    with pytest.raises(NotImplementedError, match='Exact exchange cannot be changed for'):
        dp.dalton.program.dalton_input(qcmethod, dp.Property(energy=True))


@pytest.mark.datafiles(
    pytest.DATADIR / 'hfsrdft.dal',
    pytest.DATADIR / 'hfsrdft_hfxfac.dal',
)
def test_hfsrdft_input(datafiles):
    with open((datafiles / 'hfsrdft.dal'), 'r') as file:
        reference = file.read()
    qcmethod = dp.QCMethod('HFSRDFT', 'SRXPBEGWS SRCPBEGWS')
    qcmethod.range_separation_parameter(0.4)
    input_file = dp.dalton.program.dalton_input(qcmethod, dp.Property(energy=True))
    assert input_file == reference
    # Test with Hartree-Fock like exchange
    with open((datafiles / 'hfsrdft_hfxfac.dal'), 'r') as file:
        reference = file.read()
    qcmethod = dp.QCMethod('HFSRDFT', 'SRXPBEGWS SRCPBEGWS')
    qcmethod.range_separation_parameter(0.4)
    qcmethod.exact_exchange(0.2)
    input_file = dp.dalton.program.dalton_input(qcmethod, dp.Property(energy=True))
    assert input_file == reference


@pytest.mark.datafiles(
    pytest.DATADIR / 'mp2srdft.dal',
    pytest.DATADIR / 'mp2srdft_hfxfac.dal',
)
def test_mp2srdft_input(datafiles):
    with open((datafiles / 'mp2srdft.dal'), 'r') as file:
        reference = file.read()
    qcmethod = dp.QCMethod('MP2SRDFT', 'SRXPBEGWS SRCPBEGWS')
    qcmethod.range_separation_parameter(0.4)
    input_file = dp.dalton.program.dalton_input(qcmethod, dp.Property(energy=True))
    assert input_file == reference
    # Test with Hartree-Fock like exchange
    with open((datafiles / 'mp2srdft_hfxfac.dal'), 'r') as file:
        reference = file.read()
    qcmethod = dp.QCMethod('MP2SRDFT', 'SRXPBEGWS SRCPBEGWS')
    qcmethod.range_separation_parameter(0.4)
    qcmethod.exact_exchange(0.2)
    input_file = dp.dalton.program.dalton_input(qcmethod, dp.Property(energy=True))
    assert input_file == reference


@pytest.mark.datafiles(
    pytest.DATADIR / 'He.xyz', )
def test_no_rerun(datafiles):
    """Testing that a calculation will not rerun if the output file exist."""
    with chdir(datafiles):
        filename = '0923278479ed976b987e04ed96fc83bef456f1ad'
        with open(f'{filename}.out', 'w') as file:
            file.write('\n@    Final HF energy:              -2.855160477243\n')
        with open(f'{filename}.tar.gz', 'w') as file:
            file.write('\n\n')
        output = dp.dalton.compute(dp.Molecule(input_file=(datafiles / 'He.xyz')), dp.Basis(basis='3-21G'),
                                   dp.QCMethod('HF'), dp.Property(energy=True))
    assert output.energy == pytest.approx(-2.855160477243)


@pytest.mark.datafiles(
    pytest.DATADIR / 'mp2_He.out',
    pytest.DATADIR / 'mp2_He_nosym.out',
    pytest.DATADIR / 'ci_He.out',
    pytest.DATADIR / 'ci_He_nosym.out',
)
def test_natural_occupations(datafiles):
    output = dp.dalton.OutputParser(datafiles / 'mp2_He')
    nat_occ = output.natural_occupations
    nat_occ_reference = {
        1: [1.99090161, 0.00484282], 2: [0.00141853], 3: [0.00141853], 4: [], 5: [0.00141853], 6: [], 7: [], 8: []
    }
    for key in nat_occ_reference:
        np.testing.assert_array_equal(nat_occ[key], nat_occ_reference[key])
    output = dp.dalton.OutputParser(datafiles / 'mp2_He_nosym')
    nat_occ = output.natural_occupations
    nat_occ_reference = {1: [1.99090161, 0.00484282, 0.00141853, 0.00141853, 0.00141853]}
    for key in nat_occ_reference:
        np.testing.assert_array_equal(nat_occ[key], nat_occ_reference[key])
    output = dp.dalton.OutputParser(datafiles / 'ci_He')
    nat_occ = output.natural_occupations
    nat_occ_reference = {
        1: [1.985492101, 0.008323780], 2: [0.002061373], 3: [0.002061373], 4: [], 5: [0.002061373], 6: [], 7: [],
        8: []
    }
    for key in nat_occ_reference:
        np.testing.assert_array_equal(nat_occ[key], nat_occ_reference[key])
    output = dp.dalton.OutputParser(datafiles / 'ci_He_nosym')
    nat_occ = output.natural_occupations
    nat_occ_reference = {1: [1.985492101, 0.008323780, 0.002061373, 0.002061373, 0.002061373]}
    for key in nat_occ_reference:
        np.testing.assert_array_equal(nat_occ[key], nat_occ_reference[key])


@pytest.mark.datafiles(
    pytest.DATADIR / 'empty_file.out',
    pytest.DATADIR / 'nat_occ_trigger_no_nat_occs.out',
)
def test_natural_occupation_exceptions(datafiles):
    filename = (datafiles / 'empty_file')
    output = dp.dalton.OutputParser(filename)
    with pytest.raises(Exception, match='Could not find number of symmetry elements in'):
        output.natural_occupations
    filename = (datafiles / 'nat_occ_trigger_no_nat_occs')
    output = dp.dalton.OutputParser(filename)
    with pytest.raises(Exception, match='Could not find natural occupations in'):
        output.natural_occupations


@pytest.mark.datafiles(
    pytest.DATADIR / 'casscf.dal',
    pytest.DATADIR / 'mp2srdft_Be.out',
    pytest.DATADIR / 'mp2srdft_Be.tar.gz',
    pytest.DATADIR / 'casscf_state2.dal',
    pytest.DATADIR / 'casscf_threshold.dal',
)
def test_casscf_input(datafiles):
    with open((datafiles / 'casscf.dal'), 'r') as file:
        reference = file.read()
    # Can just use the output from an mp2srdft calculation for this test.
    mp2_output = dp.dalton.OutputParser(datafiles / 'mp2srdft_Be')
    electrons, cas, inactive = dp.natural_occupation.pick_cas_by_thresholds(mp2_output.natural_occupations, 1.97,
                                                                            0.01)
    casscf = dp.QCMethod('CASSCF')
    casscf.complete_active_space(electrons, cas, inactive)
    casscf.input_orbital_coefficients(mp2_output.orbital_coefficients)
    input_file = dp.dalton.program.dalton_input(casscf, dp.Property(energy=True))
    assert input_file == reference
    # Test for another target state and another symmetry of target state
    with open((datafiles / 'casscf_state2.dal'), 'r') as file:
        reference = file.read()
    casscf = dp.QCMethod('CASSCF')
    casscf.complete_active_space(2, 2, 2)
    casscf.input_orbital_coefficients(np.array([[2.0, 0.0], [-2.0, 0.0]]))
    casscf.target_state(2, symmetry=2)
    input_file = dp.dalton.program.dalton_input(casscf, dp.Property(energy=True))
    assert input_file == reference
    # Test setting threshold
    with open((datafiles / 'casscf_threshold.dal'), 'r') as file:
        reference = file.read()
    casscf = dp.QCMethod('CASSCF')
    casscf.complete_active_space(2, 2, 2)
    casscf.input_orbital_coefficients(np.array([[2.0, 0.0], [-2.0, 0.0]]))
    casscf.target_state(1, symmetry=1)
    casscf.scf_threshold(1e-4)
    input_file = dp.dalton.program.dalton_input(casscf, dp.Property(energy=True))
    assert input_file == reference


@pytest.mark.datafiles(
    pytest.DATADIR / 'casci.dal',
    pytest.DATADIR / 'mp2srdft_Be.out',
    pytest.DATADIR / 'mp2srdft_Be.tar.gz',
    pytest.DATADIR / 'casci_state2.dal',
)
@pytest.mark.filterwarnings('ignore::UserWarning')
def test_casci_input(datafiles):
    with open((datafiles / 'casci.dal'), 'r') as file:
        reference = file.read()
    # Can just use the output from an mp2srdft calculation for this test.
    mp2_output = dp.dalton.OutputParser(datafiles / 'mp2srdft_Be')
    electrons, cas, inactive = dp.natural_occupation.pick_cas_by_thresholds(mp2_output.natural_occupations, 1.97,
                                                                            0.01)
    casscf = dp.QCMethod('CASCI')
    casscf.complete_active_space(electrons, cas, inactive)
    casscf.input_orbital_coefficients(mp2_output.orbital_coefficients)
    input_file = dp.dalton.program.dalton_input(casscf, dp.Property(energy=True))
    assert input_file == reference
    # Test for another target state and another symmetry of target state
    with open((datafiles / 'casci_state2.dal'), 'r') as file:
        reference = file.read()
    casscf = dp.QCMethod('CASCI')
    casscf.complete_active_space(2, 2, 2)
    casscf.input_orbital_coefficients(np.array([[2.0, 0.0], [-2.0, 0.0]]))
    casscf.target_state(2, symmetry=2)
    input_file = dp.dalton.program.dalton_input(casscf, dp.Property(energy=True))
    assert input_file == reference


@pytest.mark.datafiles(
    pytest.DATADIR / 'mp2srdft_Be.out', )
def test_casscf_exceptions(datafiles):
    mp2_output = dp.dalton.OutputParser(datafiles / 'mp2srdft_Be')
    electrons, cas, inactive = dp.natural_occupation.pick_cas_by_thresholds(mp2_output.natural_occupations, 1.97,
                                                                            0.01)
    casscf = dp.QCMethod('CASSCF')
    casscf.complete_active_space(electrons, cas, inactive)
    with pytest.raises(AttributeError, match='No orbitals were supplied for the CAS calculation.'):
        dp.dalton.program.dalton_input(casscf, dp.Property(energy=True))


@pytest.mark.datafiles(
    pytest.DATADIR / 'cassrdft.dal',
    pytest.DATADIR / 'mp2srdft_Be.out',
    pytest.DATADIR / 'mp2srdft_Be.tar.gz',
    pytest.DATADIR / 'cassrdft_state2.dal',
    pytest.DATADIR / 'cassrdft_hfxfac.dal',
    pytest.DATADIR / 'cassrdft_threshold.dal',
)
def test_cassrdft_input(datafiles):
    with open((datafiles / 'cassrdft.dal'), 'r') as file:
        reference = file.read()
    # Can just use the output from an mp2srdft calculation for this test.
    mp2_output = dp.dalton.OutputParser(datafiles / 'mp2srdft_Be')
    electrons, cas, inactive = dp.natural_occupation.pick_cas_by_thresholds(mp2_output.natural_occupations, 1.97,
                                                                            0.01)
    cassrdft = dp.QCMethod('CASsrDFT', 'SRXPBEGWS SRCPBEGWS')
    cassrdft.range_separation_parameter(0.4)
    cassrdft.complete_active_space(electrons, cas, inactive)
    cassrdft.input_orbital_coefficients(mp2_output.orbital_coefficients)
    input_file = dp.dalton.program.dalton_input(cassrdft, dp.Property(energy=True))
    assert input_file == reference
    # Test for another target state and another symmetry of target state
    with open((datafiles / 'cassrdft_state2.dal'), 'r') as file:
        reference = file.read()
    cassrdft = dp.QCMethod('CASSRDFT', 'SRXLDA SRCLDA')
    cassrdft.complete_active_space(2, 2, 2)
    cassrdft.range_separation_parameter(0.4)
    cassrdft.input_orbital_coefficients(np.array([[2.0, 0.0], [-2.0, 0.0]]))
    cassrdft.target_state(2, symmetry=2)
    input_file = dp.dalton.program.dalton_input(cassrdft, dp.Property(energy=True))
    assert input_file == reference
    # Test with Hartree-Fock like exchange
    with open((datafiles / 'cassrdft_hfxfac.dal'), 'r') as file:
        reference = file.read()
    cassrdft = dp.QCMethod('CASSRDFT', 'SRXPBEGWS SRCPBEGWS')
    cassrdft.complete_active_space(2, 2, 2)
    cassrdft.range_separation_parameter(0.4)
    cassrdft.exact_exchange(0.2)
    cassrdft.input_orbital_coefficients(np.array([[2.0, 0.0], [-2.0, 0.0]]))
    input_file = dp.dalton.program.dalton_input(cassrdft, dp.Property(energy=True))
    assert input_file == reference
    # Test setting threshold
    with open((datafiles / 'cassrdft_threshold.dal'), 'r') as file:
        reference = file.read()
    cassrdft = dp.QCMethod('CASSRDFT', 'SRXPBEGWS SRCPBEGWS')
    cassrdft.complete_active_space(2, 2, 2)
    cassrdft.range_separation_parameter(0.4)
    cassrdft.scf_threshold(1e-4)
    cassrdft.input_orbital_coefficients(np.array([[2.0, 0.0], [-2.0, 0.0]]))
    input_file = dp.dalton.program.dalton_input(cassrdft, dp.Property(energy=True))
    assert input_file == reference


@pytest.mark.datafiles(
    pytest.DATADIR / 'cascisrdft.dal',
    pytest.DATADIR / 'mp2srdft_Be.out',
    pytest.DATADIR / 'mp2srdft_Be.tar.gz',
)
@pytest.mark.filterwarnings('ignore::UserWarning')
def test_cascisrdft_input(datafiles):
    with open((datafiles / 'cascisrdft.dal'), 'r') as file:
        reference = file.read()
    # Can just use the output from an mp2srdft calculation for this test.
    mp2_output = dp.dalton.OutputParser(datafiles / 'mp2srdft_Be')
    electrons, cas, inactive = dp.natural_occupation.pick_cas_by_thresholds(mp2_output.natural_occupations, 1.97,
                                                                            0.01)
    cassrdft = dp.QCMethod('CASCIsrDFT', 'SRXPBEGWS SRCPBEGWS')
    cassrdft.range_separation_parameter(0.4)
    cassrdft.complete_active_space(electrons, cas, inactive)
    cassrdft.input_orbital_coefficients(mp2_output.orbital_coefficients)
    input_file = dp.dalton.program.dalton_input(cassrdft, dp.Property(energy=True))
    assert input_file == reference


@pytest.mark.datafiles(
    pytest.DATADIR / 'mp2srdft_Be.out', )
def test_cassrdft_exceptions(datafiles):
    mp2_output = dp.dalton.OutputParser(datafiles / 'mp2srdft_Be')
    electrons, cas, inactive = dp.natural_occupation.pick_cas_by_thresholds(mp2_output.natural_occupations, 1.97,
                                                                            0.01)
    cassrdft = dp.QCMethod('CASsrDFT', 'SRXPBEGWS SRCPBEGWS')
    cassrdft.range_separation_parameter(0.4)
    cassrdft.complete_active_space(electrons, cas, inactive)
    with pytest.raises(AttributeError, match='No orbitals were supplied for the CAS calculation.'):
        dp.dalton.program.dalton_input(cassrdft, dp.Property(energy=True))


@pytest.mark.datafiles(
    pytest.DATADIR / 'deleteorbitals_Ne.out',
    pytest.DATADIR / 'deleteorbitals_Ne.tar.gz',
    pytest.DATADIR / 'big_mo_coeffs.out',
    pytest.DATADIR / 'big_mo_coeffs.tar.gz',
)
def test_punch_orbitals(datafiles):
    output = dp.dalton.OutputParser(datafiles / 'deleteorbitals_Ne')
    reference = ('  0.98193507302637 -0.11438139672414  0.08851269889772  0.00000000000000\n  0.00000000000000\n '
                 '-0.06932190849722  0.52375771268112  0.49857107704165  0.00000000000000\n  0.00000000000000\n '
                 '-0.00000000000000 -0.00000000000000 -0.00000000000000  1.00000000000000\n  0.00000000000000\n  '
                 '0.00000000000000  0.00000000000000  0.00000000000000  0.00000000000000\n  1.00000000000000\n  '
                 '0.00000000000000  0.00000000000000  0.00000000000000  0.00000000000000\n  0.00000000000000\n  '
                 '0.52809446965387  0.52809446965387\n  0.00000000000000  0.00000000000000\n  0.52809446965387  '
                 '0.52809446965387\n  0.00000000000000  0.00000000000000\n  1.00000000000000\n  0.52809446965387  '
                 '0.52809446965387\n  0.00000000000000  0.00000000000000\n  1.00000000000000\n  1.00000000000000',
                 [1, 1, 1, 0, 1, 0, 0, 0])
    assert dp.dalton.program.punch_orbitals(output.orbital_coefficients) == reference
    output = dp.dalton.OutputParser(datafiles / 'big_mo_coeffs')
    reference = ('  0.01346601772099  0.06798611094381  0.11127926896129 -0.09365627542679\n -0.00940795794576\n'
                 '  13.4660177209900  67.9861109438100 111.2792689612900 -93.6562754267900\n  -9.4079579457600\n'
                 '  134.660177209900  679.861109438100 1112.792689612900 -936.562754267900\n  -94.079579457600\n'
                 '  1346.60177209900  6798.61109438100 11127.92689612900 -9365.62754267900\n  -940.79579457600\n'
                 '  13466.0177209900  67986.1109438100 111279.2689612900 -93656.2754267900\n  -9407.9579457600',
                 [0])
    assert dp.dalton.program.punch_orbitals(output.orbital_coefficients) == reference


@pytest.mark.datafiles(
    pytest.DATADIR / 'mp2srdft_Be.out',
    pytest.DATADIR / 'mp2srdft_Be.tar.gz',
)
def test_punch_orbitals_exceptions(datafiles):
    # Can just use the output from an mp2srdft calculation for this test.
    mp2_output = dp.dalton.OutputParser(datafiles / 'mp2srdft_Be')
    with pytest.raises(ValueError, match='Orbital coefficient to large, orbitals from dal file will be broken.'):
        dp.dalton.program.punch_orbitals(mp2_output.orbital_coefficients * 10**6)


@pytest.mark.datafiles(
    pytest.DATADIR / 'casscf_deletedorbitals.dal',
    pytest.DATADIR / 'deleteorbitals_Ne.out',
    pytest.DATADIR / 'deleteorbitals_Ne.tar.gz',
)
def test_casscf_deleted_orbitals(datafiles):
    with open((datafiles / 'casscf_deletedorbitals.dal'), 'r') as file:
        reference = file.read()
    # Can just use the output from an mp2srdft calculation for this test.
    mp2_output = dp.dalton.OutputParser(datafiles / 'deleteorbitals_Ne')
    electrons, cas, inactive = dp.natural_occupation.pick_cas_by_thresholds(mp2_output.natural_occupations, 1.97,
                                                                            0.01)
    casscf = dp.QCMethod('CASSCF')
    casscf.complete_active_space(electrons, cas, inactive)
    casscf.input_orbital_coefficients(mp2_output.orbital_coefficients)
    input_file = dp.dalton.program.dalton_input(casscf, dp.Property(energy=True))
    assert input_file == reference


@pytest.mark.datafiles(
    pytest.DATADIR / 'cc2_energy.out', )
def test_energy(datafiles):
    output = dp.dalton.OutputParser(datafiles / 'cc2_energy')
    reference = -243.9903563569
    assert abs(output.energy - reference) < 1e-9


@pytest.mark.datafiles(
    pytest.DATADIR / 'empty_file.out', )
def test_energy_exceptions(datafiles):
    filename = (datafiles / 'empty_file')
    output = dp.dalton.OutputParser(filename)
    with pytest.raises(Exception, match='Energy not found in'):
        output.energy


@pytest.mark.datafiles(
    pytest.DATADIR / 'input_He_nosym.out', )
def test_electronic_energy(datafiles):
    output = dp.dalton.OutputParser(datafiles / 'input_He_nosym')
    reference = -2.855160477243
    assert abs(output.electronic_energy - reference) < 1e-11


@pytest.mark.datafiles(
    pytest.DATADIR / 'input_He_nosym.out', )
def test_nuclear_repulsion_energy(datafiles):
    output = dp.dalton.OutputParser(datafiles / 'input_He_nosym')
    reference = 0.000000000000
    assert abs(output.nuclear_repulsion_energy - reference) < 1e-11


@pytest.mark.datafiles(
    pytest.DATADIR / 'empty_file.out', )
def test_nuclear_repulsion_energy_exceptions(datafiles):
    filename = (datafiles / 'empty_file')
    output = dp.dalton.OutputParser(filename)
    with pytest.raises(Exception, match='Nuclear repulsion energy not found in'):
        output.nuclear_repulsion_energy


@pytest.mark.datafiles(
    pytest.DATADIR / 'input_He_nosym.out', )
def test_num_electrons(datafiles):
    output = dp.dalton.OutputParser(datafiles / 'input_He_nosym')
    assert output.num_electrons == 2


@pytest.mark.datafiles(
    pytest.DATADIR / 'empty_file.out', )
def test_num_electrons_exceptions(datafiles):
    filename = (datafiles / 'empty_file')
    output = dp.dalton.OutputParser(filename)
    with pytest.raises(Exception, match='Number of electrons not found in'):
        output.num_electrons


@pytest.mark.datafiles(
    pytest.DATADIR / 'empty_file.out', )
def test_num_orbitals_exceptions(datafiles):
    filename = (datafiles / 'empty_file')
    output = dp.dalton.OutputParser(filename)
    with pytest.raises(Exception, match='Number of orbitals not found in'):
        output.num_orbitals


@pytest.mark.datafiles(
    pytest.DATADIR / 'empty_file.out', )
def test_num_basis_functions_exceptions(datafiles):
    filename = (datafiles / 'empty_file')
    output = dp.dalton.OutputParser(filename)
    with pytest.raises(Exception, match='Number of basis functions not found in'):
        output.num_basis_functions


@pytest.mark.datafiles(
    pytest.DATADIR / 'empty_file.out',
    pytest.DATADIR / 'exc_energy_trigger_no_exc_energy.out',
)
def test_excitation_energies_exceptions(datafiles):
    filename = (datafiles / 'empty_file')
    output = dp.dalton.OutputParser(filename)
    with pytest.raises(NotImplementedError):
        output.excitation_energies
    filename = (datafiles / 'exc_energy_trigger_no_exc_energy')
    output = dp.dalton.OutputParser(filename)
    with pytest.raises(Exception, match='Could not find excitation energies in'):
        output.excitation_energies


@pytest.mark.datafiles(
    pytest.DATADIR / 'He_opa.out', )
def test_oscillator_strengths(datafiles):
    filename = (datafiles / 'He_opa')
    output = dp.dalton.OutputParser(filename)
    reference = np.array([0.0000, 0.7542, 0.7542, 0.7542, 0.0000])
    np.testing.assert_allclose(output.oscillator_strengths, reference)


@pytest.mark.datafiles(
    pytest.DATADIR / 'empty_file.out', )
def test_oscillator_strengths_exceptions(datafiles):
    filename = (datafiles / 'empty_file')
    output = dp.dalton.OutputParser(filename)
    with pytest.raises(Exception, match='Could not find oscillator strengths in'):
        output.oscillator_strengths


@pytest.mark.datafiles(
    pytest.DATADIR / 'empty_file.out', )
def test_two_photon_strengths_exceptions(datafiles):
    filename = (datafiles / 'empty_file')
    output = dp.dalton.OutputParser(filename)
    with pytest.raises(Exception, match='Could not find two-photon strengths in'):
        output.two_photon_strengths


@pytest.mark.datafiles(
    pytest.DATADIR / 'empty_file.out', )
def test_two_photon_cross_sections_exceptions(datafiles):
    filename = (datafiles / 'empty_file')
    output = dp.dalton.OutputParser(filename)
    with pytest.raises(Exception, match='Could not find two-photon cross-sections in'):
        output.two_photon_cross_sections


@pytest.mark.datafiles(
    pytest.DATADIR / 'empty_file.out', )
def test_hyperfine_couplings_exceptions(datafiles):
    filename = (datafiles / 'empty_file')
    output = dp.dalton.OutputParser(filename)
    with pytest.raises(Exception, match='Hyperfine couplings not found in'):
        output.hyperfine_couplings


@pytest.mark.datafiles(
    pytest.DATADIR / 'singlet_excitation.dal',
    pytest.DATADIR / 'triplet_excitation_nosym.dal',
)
def test_one_photon_excitation_input(datafiles):
    with open((datafiles / 'singlet_excitation.dal'), 'r') as file:
        reference = file.read()
    prop = dp.Property()
    prop.excitation_energies(states=[1, 2, 3, 4])
    input_file = dp.dalton.program.dalton_input(dp.QCMethod('HF'), prop)
    assert input_file == reference
    with open((datafiles / 'triplet_excitation_nosym.dal'), 'r') as file:
        reference = file.read()
    prop = dp.Property()
    prop.excitation_energies(states=5, triplet=True)
    input_file = dp.dalton.program.dalton_input(dp.QCMethod('HF'), prop)
    assert input_file == reference


@pytest.mark.datafiles(
    pytest.DATADIR / 'cvs_excitation.dal', )
def test_cvs_excitation_input(datafiles):
    with open((datafiles / 'cvs_excitation.dal'), 'r') as file:
        reference = file.read()
    prop = dp.Property(excitation_energies=True)
    prop.excitation_energies(states=[4, 4, 4, 4], cvseparation=[[1], [2, 3], [4, 5, 6], [7, 8, 9, 10]])
    input_file = dp.dalton.program.dalton_input(dp.QCMethod('CCSD'), prop)
    assert input_file == reference


@pytest.mark.datafiles(
    pytest.DATADIR / 'he_triplet.out', )
def test_triplet_excitation_energies(datafiles):
    reference = np.array([25.750206, 44.047101, 44.047101, 44.047101, 120.867627])
    output = dp.dalton.OutputParser(datafiles / 'he_triplet')
    np.testing.assert_allclose(output.excitation_energies, reference)


@pytest.mark.datafiles(
    pytest.DATADIR / '2pa.dal',
    pytest.DATADIR / '2pa_nosym.dal',
)
def test_two_photon_excitation_input(datafiles):
    with open((datafiles / '2pa.dal'), 'r') as file:
        reference = file.read()
    prop = dp.Property()
    prop.two_photon_absorption(states=[1, 2, 3, 4])
    input_file = dp.dalton.program.dalton_input(dp.QCMethod('HF'), prop)
    assert input_file == reference
    with open((datafiles / '2pa_nosym.dal'), 'r') as file:
        reference = file.read()
    prop = dp.Property()
    prop.two_photon_absorption(states=5)
    input_file = dp.dalton.program.dalton_input(dp.QCMethod('HF'), prop)
    assert input_file == reference


@pytest.mark.datafiles(
    pytest.DATADIR / 'empty_file.out', )
def test_two_photon_tensor_exceptions(datafiles):
    filename = (datafiles / 'empty_file')
    output = dp.dalton.OutputParser(filename)
    with pytest.raises(Exception, match='Could not find two-photon matrix elements in'):
        output.two_photon_tensors


@pytest.mark.datafiles(
    pytest.DATADIR / 'input_He_nosym.out',
    pytest.DATADIR / 'input_He.out',
    pytest.DATADIR / 'input_water.out',
    pytest.DATADIR / 'lda_He.out',
)
def test_mo_energies(datafiles):
    reference = (np.array([-0.91414794, 1.39744169, 2.52437201, 2.52437201, 2.52437201]),
                 {'A': np.array([-0.91414794, 1.39744169, 2.52437201, 2.52437201, 2.52437201])})
    output = dp.dalton.OutputParser(datafiles / 'input_He_nosym')
    np.testing.assert_allclose(output.mo_energies[0], reference[0])
    for key in reference[1]:
        np.testing.assert_allclose(output.mo_energies[1][key], reference[1][key])
    reference = (np.array([-0.91414794, 1.39744169, 2.52437201, 2.52437201, 2.52437201]), {
        'Ag': np.array([-0.91414794, 1.39744169]), 'B3u': np.array([2.52437201]), 'B2u': np.array([2.52437201]),
        'B1g': np.array([]), 'B1u': np.array([2.52437201]), 'B2g': np.array([]), 'B3g': np.array([]),
        'Au': np.array([])
    })
    output = dp.dalton.OutputParser(datafiles / 'input_He')
    np.testing.assert_allclose(output.mo_energies[0], reference[0])
    for key in reference[1]:
        np.testing.assert_allclose(output.mo_energies[1][key], reference[1][key])
    reference = (np.array([
        -20.55044684, -1.33676014, -0.69922661, -0.56669188, -0.49316978, 0.18556821, 0.25626708, 0.7891751,
        0.85462657, 1.16354432, 1.20037197, 1.2533532, 1.44410421, 1.47645007, 1.67439558, 1.86717135, 1.93528952,
        2.45289941, 2.49065047, 3.28600052, 3.33878102, 3.51087467, 3.86555321, 4.14788955
    ]), {
        'A\'':
            np.array([
                -20.55044684, -1.33676014, -0.69922661, -0.56669188, 0.18556821, 0.25626708, 0.78917510,
                0.85462657, 1.16354432, 1.25335320, 1.44410421, 1.86717135, 1.93528952, 2.45289941, 2.49065047,
                3.51087467, 3.86555321, 4.14788955
            ]), 'A"':
                np.array([-0.49316978, 1.20037197, 1.47645007, 1.67439558, 3.28600052, 3.33878102])
    })
    output = dp.dalton.OutputParser(datafiles / 'input_water')
    np.testing.assert_allclose(output.mo_energies[0], reference[0])
    for key in reference[1]:
        np.testing.assert_allclose(output.mo_energies[1][key], reference[1][key])
    reference = (np.array([-0.48864135]), {'A': np.array([-0.48864135])})
    output = dp.dalton.OutputParser(datafiles / 'lda_He')
    np.testing.assert_allclose(output.mo_energies[0], reference[0])
    for key in reference[1]:
        np.testing.assert_allclose(output.mo_energies[1][key], reference[1][key])


@pytest.mark.datafiles(
    pytest.DATADIR / 'mo_energy_trigger_no_orb_energy.out',
    pytest.DATADIR / 'empty_file.out',
)
def test_mo_energies_exceptions(datafiles):
    filename = (datafiles / 'mo_energy_trigger_no_orb_energy')
    output = dp.dalton.OutputParser(filename)
    with pytest.raises(Exception, match='Could not find molecular orbital energies in'):
        output.mo_energies
    filename = (datafiles / 'empty_file')
    output = dp.dalton.OutputParser(filename)
    with pytest.raises(Exception, match='Could not find Abelian symmetry species in'):
        output.mo_energies


@pytest.mark.datafiles(
    pytest.DATADIR / 'input_water.out', )
def test_lumo_energy(datafiles):
    reference = (0.18556821, 'A\'')
    output = dp.dalton.OutputParser(datafiles / 'input_water')
    np.testing.assert_allclose(output.lumo_energy[0], reference[0])
    assert output.lumo_energy[1] == reference[1]


@pytest.mark.datafiles(
    pytest.DATADIR / 'mo_energy_trigger_no_orb_energy.out', )
def test_lumo_energy_exceptions(datafiles):
    filename = (datafiles / 'mo_energy_trigger_no_orb_energy')
    output = dp.dalton.OutputParser(filename)
    with pytest.raises(Exception, match='Could not find LUMO energy in'):
        output.lumo_energy


@pytest.mark.datafiles(
    pytest.DATADIR / 'input_water.out', )
def test_homo_energy(datafiles):
    reference = (-0.49316978, 'A"')
    output = dp.dalton.OutputParser(datafiles / 'input_water')
    np.testing.assert_allclose(output.homo_energy[0], reference[0])
    assert output.homo_energy[1] == reference[1]


@pytest.mark.datafiles(
    pytest.DATADIR / 'mo_energy_trigger_no_orb_energy.out', )
def test_homo_energy_exceptions(datafiles):
    filename = (datafiles / 'mo_energy_trigger_no_orb_energy')
    output = dp.dalton.OutputParser(filename)
    with pytest.raises(Exception, match='Could not find HOMO energy in'):
        output.homo_energy


@pytest.mark.datafiles(
    pytest.DATADIR / 'molgrad_dalton.dal', )
def test_molecular_gradients_input(datafiles):
    with open((datafiles / 'molgrad_dalton.dal'), 'r') as file:
        reference = file.read()
    input_file = dp.dalton.program.dalton_input(dp.QCMethod('HF'), dp.Property(gradients=True))
    assert input_file == reference


@pytest.mark.datafiles(
    pytest.DATADIR / 'molgrad_dalton.out', )
def test_molecular_gradients(datafiles):
    reference = np.array([[0.0383576457, -0.0, 0.0], [-0.0191788228, 0.0, -0.1423599474],
                          [-0.0191788228, 0.0, 0.1423599474]])
    output = dp.dalton.OutputParser(datafiles / 'molgrad_dalton')
    np.testing.assert_allclose(output.gradients, reference)


@pytest.mark.datafiles(
    pytest.DATADIR / 'molgrad_trigger_no_num_atoms_dalton.out',
    pytest.DATADIR / 'molgrad_trigger_no_molgrad_dalton.out',
    pytest.DATADIR / 'molgrad_sym_dalton.out',
)
def test_molecular_gradients_exceptions(datafiles):
    filename = (datafiles / 'molgrad_trigger_no_num_atoms_dalton')
    output = dp.dalton.OutputParser(filename)
    with pytest.raises(Exception, match='Number of atoms not found in'):
        output.gradients
    filename = (datafiles / 'molgrad_trigger_no_molgrad_dalton')
    output = dp.dalton.OutputParser(filename)
    with pytest.raises(Exception, match='Molecular gradients not found in'):
        output.gradients
    output = dp.dalton.OutputParser(datafiles / 'molgrad_sym_dalton')
    with pytest.raises(Exception,
                       match='Extraction of molecular gradients from Dalton output file is only'
                       ' supported without use of symmetry.'):
        output.gradients


@pytest.mark.datafiles(
    pytest.DATADIR / 'empty_file.out', )
def test_final_geometry_exceptions(datafiles):
    filename = (datafiles / 'empty_file')
    output = dp.dalton.OutputParser(filename)
    with pytest.raises(NotImplementedError, match='final geometry is not implemented for Dalton'):
        output.final_geometry


@pytest.mark.parametrize(
    'method, properties, expected',
    [
        (
            {'qc_method': 'HF', 'scf_threshold': 1e-6},
            {'energy': True},
            textwrap.dedent("""\
                **DALTON INPUT
                .DIRECT
                .RUN WAVE FUNCTION
                **WAVE FUNCTIONS
                .HF
                *SCF INPUT
                .THRESH
                1e-06
                *ORBITAL INPUT
                .PUNCHOUTORBITALS
                **END OF DALTON INPUT
                """),
        ),
        (
            {'qc_method': 'DFT', 'xc_functional': 'B3LYP'},
            {'energy': True},
            textwrap.dedent("""\
                **DALTON INPUT
                .DIRECT
                .RUN WAVE FUNCTION
                **WAVE FUNCTIONS
                .DFT
                B3LYP
                *ORBITAL INPUT
                .PUNCHOUTORBITALS
                **END OF DALTON INPUT
                """),
        ),
        (
            {'qc_method': 'DFT', 'xc_functional': 'BP86'},
            {'hyperfine_couplings': {'atoms': [1, 4]}},
            textwrap.dedent("""\
                **DALTON INPUT
                .DIRECT
                .RUN RESPONSE
                **WAVE FUNCTIONS
                .DFT
                BP86
                *ORBITAL INPUT
                .PUNCHOUTORBITALS
                **INTEGRALS
                .FC
                **RESPONSE
                .TRPFLG
                *ESR
                .ATOMS
                2
                1 4
                .FCCALC
                **END OF DALTON INPUT
                """),
        ),
        (
            {'qc_method': 'MP2'},
            {'energy': True},
            textwrap.dedent("""\
                **DALTON INPUT
                .DIRECT
                .FCKTRA
                .RUN WAVE FUNCTION
                **WAVE FUNCTIONS
                .HF
                .MP2
                *ORBITAL INPUT
                .PUNCHOUTORBITALS
                **END OF DALTON INPUT
                """),
        ),
    ],
    ids=['hf-energy', 'dft-energy', 'dft-hyperfine_couplings', 'mp2-energy'],
)
def test_dalton_input(method, properties, expected):
    generated = dp.dalton.program.dalton_input(dp.qcmethod.QCMethod(**method), dp.property.Property(**properties))
    assert generated == expected


@pytest.mark.datafiles(
    pytest.DATADIR / '1PA_response.out', )
def test_response_parsing(datafiles):
    filename = (datafiles / '1PA_response')
    result = dp.dalton.OutputParser(filename)
    np.testing.assert_allclose(result.excitation_energies,
                               np.array([4.22288629, 5.14603065, 5.40322288, 5.72037458, 6.07934119]))
    np.testing.assert_allclose(result.oscillator_strengths,
                               np.array([0.57282922, 0.02465654, 0.01786578, 0.01074149, 0.0016151]),
                               atol=1e-8)


@pytest.mark.datafiles(
    pytest.DATADIR / 'cvs_cc.out', )
@pytest.mark.filterwarnings('ignore::UserWarning')
def test_cvs_cc_parsing(datafiles):
    filename = (datafiles / 'cvs_cc')
    result = dp.dalton.OutputParser(filename)
    np.testing.assert_allclose(np.array([14.92878, 18.43781, 29.79227, 30.24133, 32.37882]),
                               result.excitation_energies)
    np.testing.assert_allclose(np.array([0.29331132, 0.10844814, 0.13033753, 0.00000000, 0.00000788]),
                               result.oscillator_strengths)


@pytest.mark.datafiles(
    pytest.DATADIR / 'empty_file.out', )
def test_notimplementederror(datafiles):
    filename = (datafiles / 'empty_file')
    output = dp.dalton.OutputParser(filename)
    with pytest.raises(NotImplementedError):
        output.hessian
    with pytest.raises(NotImplementedError):
        output.dipole_gradients
    with pytest.raises(NotImplementedError):
        output.polarizability_gradients
    with pytest.raises(NotImplementedError):
        output.final_geometry
